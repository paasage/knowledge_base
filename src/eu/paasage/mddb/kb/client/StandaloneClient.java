/* Copyright (C) 2015 KYRIAKOS KRITIKOS <kritikos@ics.forth.gr> */

/* This Source Code Form is subject to the terms of the Mozilla Public 
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/ 
 */

package eu.paasage.mddb.kb.client;

import java.io.File;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Hashtable;
import java.util.Iterator;

import org.eclipse.emf.cdo.view.CDOView;
import org.kie.api.KieBaseConfiguration;
import org.kie.api.io.Resource;
import org.kie.api.io.ResourceType;
import org.kie.api.runtime.rule.QueryResults;
import org.kie.api.runtime.rule.QueryResultsRow;
import org.kie.internal.KnowledgeBase;
import org.kie.internal.KnowledgeBaseFactory;
import org.kie.internal.builder.KnowledgeBuilder;
import org.kie.internal.builder.KnowledgeBuilderFactory;
import org.kie.internal.definition.KnowledgePackage;
import org.kie.internal.io.ResourceFactory;
import org.kie.internal.runtime.StatefulKnowledgeSession;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import eu.paasage.camel.CamelFactory;
import eu.paasage.camel.CamelModel;
import eu.paasage.mddb.cdo.client.CDOClient;
import eu.paasage.mddb.kb.domain.CDOUtils;

public class StandaloneClient {
	private CDOClient client = null;
	private CDOView view = null;
	private Hashtable<String,KnowledgeBase> nameToKB = new Hashtable<String,KnowledgeBase>();
	private Hashtable<String,Hashtable<String,StatefulKnowledgeSession>> nameToSession = new Hashtable<String,Hashtable<String,StatefulKnowledgeSession>>();
	
	private static Logger logger = LoggerFactory.getLogger(StandaloneClient.class);
	
	public StandaloneClient(){
		logger.info("initiating cdo components");
		initCDOComponents();
	}
	
	private void initCDOComponents(){
		client = new CDOClient();
		view = client.openView();
		logger.info("cdo components created");
	}
	
	public boolean createKB(String kbName){
		logger.info("Creating KB: " + kbName);
		KnowledgeBase kBase = nameToKB.get(kbName);
		if (kBase == null){
			KieBaseConfiguration config = KnowledgeBaseFactory.newKnowledgeBaseConfiguration();
			kBase = KnowledgeBaseFactory.newKnowledgeBase(config);
			nameToKB.put(kbName,kBase);
			return true;
		}
		else logger.error("KB with : " + kbName + " already exists");
		return false;
	}
	
	public boolean createSession(String sessionName, String kbName){
		logger.info("Creating session: " + sessionName + " for KB: " + kbName);
		KnowledgeBase kBase = nameToKB.get(kbName);
		if (kBase != null){
			Hashtable<String,StatefulKnowledgeSession> sessions = nameToSession.get(kbName);
			StatefulKnowledgeSession ksession = null;
			if (sessions != null){
				ksession = sessions.get(sessionName);
			}
			if (ksession == null){
				ksession = kBase.newStatefulKnowledgeSession();
				if (sessions == null){
					sessions = new Hashtable<String,StatefulKnowledgeSession>();
				}
				sessions.put(sessionName, ksession);
				nameToSession.put(kbName, sessions);
				return true;
			}
			else logger.error("Session with : " + sessionName + " already exists in KB: " + kbName);
		}
		else logger.error("KB with : " + kbName + " does not exist");
		return false;
	}
	
	public QueryResults runQuery(String sessionName, String kbName, String query, Object[] parameters){
		logger.info("Running query: " + query + " with object array: " + parameters + " for session: " + sessionName + " of KB: " + kbName);
		KnowledgeBase kBase = nameToKB.get(kbName);
		if (kBase != null){
			Hashtable<String,StatefulKnowledgeSession> sessions = nameToSession.get(kbName);
			StatefulKnowledgeSession ksession = null;
			if (sessions != null){
				ksession = sessions.get(sessionName);
				if (ksession != null){
					return ksession.getQueryResults(query, parameters);
				}
				else logger.error("Session with : " + sessionName + " does not exist in KB: " + kbName);
			}
		}
		else logger.error("KB with : " + kbName + " does not exist");
		return null;
	}
	
	public boolean deleteKB(String kbName){
		logger.info("Deleting KB: " + kbName);
		KnowledgeBase kBase = nameToKB.get(kbName);
		if (kBase != null){
			Hashtable<String,StatefulKnowledgeSession> sessions = nameToSession.get(kbName);
			if (sessions != null){
				for (StatefulKnowledgeSession ksession: sessions.values()) ksession.dispose();
			}
			nameToSession.remove(kbName);
			nameToKB.remove(kbName);
			return true;
		}
		else logger.error("KB with : " + kbName + " does not exist");
		return false;
	}
	
	public boolean deleteSession(String sessionName, String kbName){
		logger.info("Deleting Session: " + sessionName + " for KB: " + kbName);
		KnowledgeBase kBase = nameToKB.get(kbName);
		if (kBase != null){
			Hashtable<String,StatefulKnowledgeSession> sessions = nameToSession.get(kbName);
			if (sessions != null){
				StatefulKnowledgeSession ksession = sessions.remove(sessionName);
				if (ksession != null){
					ksession.dispose();
					return true;
				}
				else logger.error("Session with : " + sessionName + " does not exist in KB: " + kbName);
			}
		}
		else logger.error("KB with : " + kbName + " does not exist");
		return false;
	}
	
	public boolean fireRules(String sessionName, String kbName){
		logger.info("Firing rules for Session: " + sessionName + " for KB: " + kbName);
		KnowledgeBase kBase = nameToKB.get(kbName);
		if (kBase != null){
			Hashtable<String,StatefulKnowledgeSession> sessions = nameToSession.get(kbName);
			if (sessions != null){
				StatefulKnowledgeSession ksession = sessions.get(sessionName);
				if (ksession != null){
					ksession.fireAllRules();
				}
				else logger.error("Session with : " + sessionName + " does not exist in KB: " + kbName);
			}
		}
		else logger.error("KB with : " + kbName + " does not exist");
		return false;
	}
	
	public boolean addRules(String kbName, String dirPath){
		logger.info("Adding rules for KB: " + kbName);
		KnowledgeBase kBase = nameToKB.get(kbName);
		if (kBase != null){
			logger.info("Updating user base: " + kbName);
			KnowledgeBuilder kbuilder = KnowledgeBuilderFactory.newKnowledgeBuilder();
			
			try{
				File basicDir = new File(dirPath);
				if (basicDir.exists()){
					logger.info("dir exists");
					File[] files = basicDir.listFiles();
					for (File f: files){
						Resource res = ResourceFactory.newFileResource(f);
						logger.info("Adding resource: " + res);
						kbuilder.add(res,ResourceType.DRL);
					}
				}
				else logger.error("Directory denoted by path: " + dirPath + " does not exist");
			}
			catch(Exception e){
				logger.error("Something went wrong with the addition of DRL resources: ",e);
				return false;
			}
		
			if( kbuilder.hasErrors() ) {
			    logger.error("The addition of a resource lead to errors: " + kbuilder.getErrors() );
			    return false;
			}
		
			Collection<KnowledgePackage> kpkgs = kbuilder.getKnowledgePackages();
			kBase.addKnowledgePackages(kpkgs);
			logger.info("Updated base: " + kbName);
			logger.info("Added basic knowledge packages");
			return true;
		}
		else logger.error("KB with : " + kbName + " does not exist");
		return false;
	}
	
	public boolean addObjects(String sessionName, String kbName, Object[] objects){
		logger.info("Adding objects: " + objects + " for Session: " + sessionName + " for KB: " + kbName);
		KnowledgeBase kBase = nameToKB.get(kbName);
		if (kBase != null){
			Hashtable<String,StatefulKnowledgeSession> sessions = nameToSession.get(kbName);
			if (sessions != null){
				StatefulKnowledgeSession ksession = sessions.get(sessionName);
				if (ksession != null){
					for (Object o: objects) ksession.insert(o);
					return true;
				}
				else logger.error("Session with : " + sessionName + " does not exist in KB: " + kbName);
			}
		}
		else logger.error("KB with : " + kbName + " does not exist");
		return false;
	}
	
	public boolean setGlobal(String sessionName, String kbName, String globalName, Object object){
		logger.info("Setting global: " + globalName + " " + " for Session: " + sessionName + " for KB: " + kbName);
		KnowledgeBase kBase = nameToKB.get(kbName);
		if (kBase != null){
			Hashtable<String,StatefulKnowledgeSession> sessions = nameToSession.get(kbName);
			if (sessions != null){
				StatefulKnowledgeSession ksession = sessions.get(sessionName);
				if (ksession != null){
					ksession.setGlobal(globalName, object);
					return true;
				}
				else logger.error("Session with : " + sessionName + " does not exist in KB: " + kbName);
			}
		}
		else logger.error("KB with : " + kbName + " does not exist");
		return false;
	}
	
	public Object[] getObjects(String sessionName, String kbName){
		logger.info("Getting objects for Session: " + sessionName + " for KB: " + kbName);
		KnowledgeBase kBase = nameToKB.get(kbName);
		if (kBase != null){
			Hashtable<String,StatefulKnowledgeSession> sessions = nameToSession.get(kbName);
			if (sessions != null){
				StatefulKnowledgeSession ksession = sessions.get(sessionName);
				if (ksession != null){
					return ksession.getObjects().toArray();
				}
				else logger.error("Session with : " + sessionName + " does not exist in KB: " + kbName);
			}
		}
		else logger.error("KB with : " + kbName + " does not exist");
		return null;
	}
	
	//This method is required just for printing the results obtained from the execution of a query 
	private static void printQueryResults(QueryResults qr){
		if (qr != null){
			Iterator<QueryResultsRow> results = qr.iterator();
			String[] identifiers = qr.getIdentifiers();
			logger.info("Identifiers: " + identifiers);
			while (results.hasNext()){
				QueryResultsRow qrr = results.next();
				logger.info("Row [");
				for (String s: identifiers){
					Object o = qrr.get(s);
					System.out.print(o.toString() + ",");
				}
				logger.info("]");
			}
		}
		else logger.info("Null query results");
	}
	
	public static void main(String[] args){
		//Create scenario for the firing of rules and the issuing of queries
		try{
			ScenarioCreator.loadScenario(true);
		}
		catch(Exception e){
			e.printStackTrace();
		}
		//Name of session and kb to construct
		String sessionName = "mySession";
		String kbName = "myKB";
		//Create the KB client
		logger.info("Creating StandaloneClient");
		StandaloneClient sc = new StandaloneClient();
		//Create a new KB
		boolean ok = sc.createKB(kbName);
		if (!ok) logger.info("Something went wrong with the creation of the KB: " + kbName);
		//Create a new session for this KB
		ok = sc.createSession(sessionName,kbName);
		if (!ok) logger.info("Something went wrong with the creation of the session: " + sessionName + " on KB: " + kbName);
		//Add rules to the KB mapping to particular queries
		ok = sc.addRules(kbName,"input");
		if (!ok) logger.info("Something went wrong with the addition of rules in KB: " + kbName);
		//Set globals for the session
		ok = sc.setGlobal(sessionName,kbName,"view",sc.view);
		if (!ok) logger.info("Something went wrong with the setting of global 'view' for session: " + sessionName + " in KB: " + kbName);
		ok = sc.setGlobal(sessionName,kbName,"threshold",new Double(0.5));
		if (!ok) logger.info("Something went wrong with the setting of global 'threshold' for session: " + sessionName + " in KB: " + kbName);
		//Add a camel model to the session
		ArrayList al = new ArrayList();
		//Add the components here
		CamelModel cm = CamelFactory.eINSTANCE.createCamelModel();
		cm.setName("CAMEL MODEL");
		al.add(cm);
		ok = sc.addObjects(sessionName,kbName,al.toArray());
		if (!ok) logger.info("Something went wrong with the addition of objects in session: " + sessionName + "of the KB: " + kbName);
		//Fire all rules for this session
		ok = sc.fireRules(sessionName, kbName);
		if (!ok) logger.info("Something went wrong with the firing of rules in session: " + sessionName + "of the KB: " + kbName);
		//Get objects from session
		Object[] objects = sc.getObjects(sessionName,kbName);
		if (objects != null){
			for (Object o: objects) logger.info("Got object: " + o.toString());
		}
		else logger.info("No objects were obtained or something went wrong with session: " + sessionName + " of KB: " + kbName);
		//Issue a particular query to the session, obtain back the results and print them
		objects = new Object[1];
		objects[0] = CDOUtils.getObject("IC01","InternalComponent");
		//objects[1] = CDOUtils.getObject("SLO_COMP_ET","ServiceLevelObjective");
		QueryResults qr = sc.runQuery(sessionName,kbName,"componentMatch",objects);
		printQueryResults(qr);
		//Delete the session
		ok = sc.deleteSession(sessionName,kbName);
		if (!ok) logger.info("Something went wrong with the deletion of session: " + sessionName + "of the KB: " + kbName);
		//Delete the KB created
		ok = sc.deleteKB(kbName);
		if (!ok) logger.info("Something went wrong with the deletion of KB: " + kbName);
		System.exit(1);
	}
}
