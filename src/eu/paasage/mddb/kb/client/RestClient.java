/* Copyright (C) 2015 KYRIAKOS KRITIKOS <kritikos@ics.forth.gr> */

/* This Source Code Form is subject to the terms of the Mozilla Public 
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/ 
 */

package eu.paasage.mddb.kb.client;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.StringWriter;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;

import org.eclipse.emf.cdo.common.id.CDOID;
import org.w3c.dom.Node;

import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.api.client.filter.HTTPBasicAuthFilter;
import com.sun.jersey.core.util.MultivaluedMapImpl;
import com.sun.jersey.multipart.FormDataBodyPart;
import com.sun.jersey.multipart.FormDataMultiPart;

import eu.paasage.mddb.kb.domain.ApplicationMatch;
import eu.paasage.mddb.kb.domain.BestApplicationDeployment;
import eu.paasage.mddb.kb.domain.BestComponentDeployment;
import eu.paasage.mddb.kb.domain.CDOIDAdapter;
import eu.paasage.mddb.kb.domain.CDOIDSet;
import eu.paasage.mddb.kb.domain.CDOUtils;
import eu.paasage.mddb.kb.domain.ComponentMatch;
import eu.paasage.mddb.kb.domain.KBContent;
import eu.paasage.mddb.kb.domain.MyQueryParameter;
import eu.paasage.mddb.kb.domain.MyQueryResults;
import eu.paasage.mddb.kb.domain.MyResultColumn;
import eu.paasage.mddb.kb.domain.MyResultRow;
import eu.paasage.mddb.kb.domain.SuccessfulApplicationDeployment;
import eu.paasage.mddb.kb.domain.SuccessfulComponentDeployment;
import eu.paasage.mddb.kb.domain.Wildcard;

class RestClient{ 
		private String host,port,userName,password,url;
		
		private boolean logging = false;
		private static org.apache.log4j.Logger logger;
				
		//A static parameter that maps to the configuration directory that contains the properties file of the CDOClient
		private static final String ENV_CONFIG="PAASAGE_CONFIG_DIR";
		//A static parameter that maps to a default path where the properties file of the CDOClient can be found
	    private static final String DEFAULT_PAASAGE_CONFIG_DIR=".paasage";
	    //A static parameter that maps to the name of the properties file
	    private static final String PROPERTY_FILENAME="eu.paasage.mddb.kb.client.properties";
	    
	    private static final String propertyFilePath;
	    
	    static {
	    	logger = org.apache.log4j.Logger.getLogger(RestClient.class);
	    	propertyFilePath = retrieveConfigurationDirectoryFullPath();
	    }
		
		/* This method is used in order to retrieve the full path to the 
		 * configuration directory which contains the properties file of the 
		 * RestClient (which contains information to connect to the Rest Server)
		 */
		private static String retrieveConfigurationDirectoryFullPath()
	    {
	        String propertyFilePath = System.getenv(ENV_CONFIG);
	        logger.info("Got path: " + propertyFilePath);
	        
	     // enable passing the configuration directory through -Deu.paasage.configdir=PATH JVM option
	        if (propertyFilePath == null) {
	          propertyFilePath = System.getProperty("eu.paasage.configdir");
	          logger.info("Got path: " + propertyFilePath);
	        }
	        
	        if (propertyFilePath == null)
	        {
	            String home = System.getProperty("user.home");
	            Path homePath = Paths.get(home);
	            propertyFilePath = homePath.resolve(DEFAULT_PAASAGE_CONFIG_DIR).toAbsolutePath().toString();
	        }
	        return propertyFilePath;
	    }
		
		/* This method is used to find the path to the property file which specifies
		 * the connection information to the Rest Server 
		 */
	    private String retrievePropertiesFilePath(String propertiesFileName)
	    {
	        Path configPath = Paths.get(propertyFilePath);
	        return configPath.resolve(propertiesFileName).toAbsolutePath().toString();
	    }
	    
	    /* This method is used in order to load a property file of the CDOClient
	     * which contains the information needed to connect to the CDOServer
	     */
	    private Properties loadPropertyFile()
	    {
	        String propertyPath = retrievePropertiesFilePath(PROPERTY_FILENAME);
	        Properties props = new Properties();
	        try {
	            props.load(new FileInputStream(propertyPath));
	        } catch (java.io.IOException e) {
	            //TODO: fill props with default values for componenet
	            props.put("log4j.rootLogger","info, stdout");
	            props.put("log4j.appender.stdout"                         ,"org.apache.log4j.ConsoleAppender");
	            props.put("log4j.appender.stdout.Target"                  ,"System.out");
	            props.put("log4j.appender.stdout.layout"                  ,"org.apache.log4j.PatternLayout");
	            props.put("log4j.appender.stdout.layout.ConversionPattern","%d{ABSOLUTE} %5p %c{1}:%L - %m%n");
	        }
	        return props;
	    }

		/* This method is called in order to get the connection information
		 * that will be used in order to be able to connect correctly to the
		 * CDO Server and create the respective CDOSession
		 */
		private void getConnectionInformation(){
			Properties props = loadPropertyFile();
			host = props.getProperty("host");
			port = props.getProperty("port");
			userName = props.getProperty("username");
			password = props.getProperty("password");
			String log = props.getProperty("logging");
			if (log == null || log.equals("off")) logging = false;
			else if (log.equals("on")) logging = true;
			logger.info("Got host: " + host + " and port: " + port + " and logging: " + logging);
		}

		
		public RestClient(){
			getConnectionInformation();
			url = "http://" + host + ":" + port + "/ksession-manager-1.0-SNAPSHOT/rest/knowledge/";
			logger.debug("url is: " + url);
		}
		
		private void createSession(String sessionName, String kbName){
			logger.debug("Creating a StatefulKnowledgeSession with name: " + sessionName + "based on KB: " + kbName + " ...");
			Client c = Client.create();
			c.setFollowRedirects(true);
			HTTPBasicAuthFilter filter = new HTTPBasicAuthFilter(userName,password);
			c.addFilter(filter);
			WebResource r = c.resource(url + "createSession");
			
			//Create a MultivaluedMap with one field/parameter
			MultivaluedMap<String, String> params = new MultivaluedMapImpl();
			params.add("sessionName",sessionName);
			params.add("kbName",kbName);
		    
		    //Running the method with the query parameters posed and the output media type 
		    //set and getting back the result
		    ClientResponse response = null;
		    response = r.queryParams(params).post(ClientResponse.class);
		    logger.debug("Response: " + response.getEntity(String.class));
		    logger.debug("... Creation of StatefulKnowledgeSession: " + sessionName + " finished");
		}
		
		private void createKB(String kbName, boolean basic){
			logger.debug("Creating a KB with name: " + kbName + " ...");
			Client c = Client.create();
			c.setFollowRedirects(true);
			
			HTTPBasicAuthFilter filter = new HTTPBasicAuthFilter(userName,password);
			c.addFilter(filter);
			WebResource r = c.resource(url + "createKB");
			
			//Create a MultivaluedMap with one field/parameter
			MultivaluedMap<String, String> params = new MultivaluedMapImpl();
			params.add("kbName",kbName);
			params.add("basic","" + basic);
		    
		    //Running the method with the query parameters posed and the output media type 
		    //set and getting back the result
		    ClientResponse response = null;
		    response = r.queryParams(params).post(ClientResponse.class);
		    logger.debug("Response: " + response.getEntity(String.class));
		    logger.debug("... Creation of KB: " + kbName + " finished");
		}
		
		private void addRules(String kbName, String fileName){
			logger.debug("Adding rules to KB: " + kbName + " ...");
			Client c = Client.create();
			c.setFollowRedirects(true);
			HTTPBasicAuthFilter filter = new HTTPBasicAuthFilter(userName,password);
			c.addFilter(filter);
			WebResource r = c.resource(url + "addRules");
			
			//Creating a FileInputStream from the input .drl file
		    InputStream fis = null;
		    try{
		    	fis = new FileInputStream("input/" + fileName);
		    }
		    catch(Exception e){
		    	logger.error("", e);
		    }
		    //Create a MultiPart Data Form with one field mapping to the name of the KB
		    FormDataMultiPart form = new FormDataMultiPart();
		    form.field("kbName", kbName);

		    //Add to this form a data body part pertaining to the InputStream of the .drl file
		    FormDataBodyPart fdp = new FormDataBodyPart("drlFile",
		      fis, MediaType.TEXT_PLAIN_TYPE);
		    form.bodyPart(fdp);
		    
		    //Running the method with the query parameters posed and the output media type 
		    //set and getting back the result
		    ClientResponse response = null;
		    response = r.type(MediaType.MULTIPART_FORM_DATA).post(ClientResponse.class,form);
		    logger.debug("Response: " + response.getEntity(String.class));
		    logger.debug("... Addition of rules to KB: " + kbName + " finished");
		}
		
		private void addObjects(String sessionName, KBContent content){
			logger.debug("Adding objects to Session: " + sessionName + " ...");
			Client c = Client.create();
			c.setFollowRedirects(true);
			HTTPBasicAuthFilter filter = new HTTPBasicAuthFilter(userName,password);
			c.addFilter(filter);
			WebResource r = c.resource(url + "addObjects");
			
		    //Create a MultiPart Data Form with two field for the name of the session and the list of objects to be added
			//Please be careful to add Objects that are part of the domain (see classes in eu.paasage.domain.knowledge)
		    FormDataMultiPart form = new FormDataMultiPart();
		    form.field("sessionName", sessionName);
		    form.field("list",content,MediaType.APPLICATION_XML_TYPE);
		    
		    //Running the method with the query parameters posed and the media type 
		    //set and getting back the result
		    ClientResponse response = null;
		    response = r.type(MediaType.MULTIPART_FORM_DATA).post(ClientResponse.class,form);
		    logger.debug("Response: " + response.getEntity(String.class));
		    logger.debug("... Addition of objects to session: " + sessionName + " finished");
		}
		
		private KBContent getObjects(String sessionName){
			KBContent content = null;
			logger.debug("Obtaining objects from StatefulKnowledgeSession with name: " + sessionName + " ...");
			Client c = Client.create();
			c.setFollowRedirects(true);
			HTTPBasicAuthFilter filter = new HTTPBasicAuthFilter(userName,password);
			c.addFilter(filter);
			WebResource r = c.resource(url + "getObjects");
			
			//Create a MultivaluedMap with one field/parameter
			MultivaluedMap<String, String> params = new MultivaluedMapImpl();
			params.add("sessionName",sessionName);
		    
		    //Running the method with the query parameters posed 
		    //and getting back the result
		    ClientResponse response = null;
		    response = r.queryParams(params).post(ClientResponse.class);
		    //System.out.println("Response: " + response.getEntity(String.class));
		    int code = response.getStatus();
		    if (code < 300){
		    	content = response.getEntity(KBContent.class);
		    }
		    logger.debug("... Retrieval of objects from StatefulKnowledgeSession: " + sessionName + " finished");
		    return content;
		}
		
		private void fireRules(String sessionName){
			logger.debug("Firing rules for StatefulKnowledgeSession with name: " + sessionName + " ...");
			Client c = Client.create();
			c.setFollowRedirects(true);
			HTTPBasicAuthFilter filter = new HTTPBasicAuthFilter(userName,password);
			c.addFilter(filter);
			WebResource r = c.resource(url + "fireRules");
			
			//Create a MultiPart Data Form with one field/parameter
		    FormDataMultiPart form = new FormDataMultiPart();
		    form.field("sessionName", sessionName);
		    
		    //Running the method with the query parameters posed and the media type 
		    //set and getting back the result
		    ClientResponse response = null;
		    response = r.type(MediaType.MULTIPART_FORM_DATA).post(ClientResponse.class,form);
		    logger.debug("Response: " + response.getEntity(String.class));
		    logger.debug("... Firing of rules for  StatefulKnowledgeSession: " + sessionName + " finished");
		}
		
		private MyQueryResults runQuery(String sessionName, String query, List<MyQueryParameter> parameters){
			logger.debug("Running query: " + query + " for StatefulKnowledgeSession with name: " + sessionName + " ...");
			Client c = Client.create();
			c.setFollowRedirects(true);
			HTTPBasicAuthFilter filter = new HTTPBasicAuthFilter(userName,password);
			c.addFilter(filter);
			WebResource r = c.resource(url + "runQuery");
			
			//Create a MultiPart Data Form with at least two field/parameters: name of the session and name of the query
		    FormDataMultiPart form = new FormDataMultiPart();
		    form.field("sessionName", sessionName);
		    form.field("queryName", query);
		    //Add "parameters" fields to the form that map to the input parameters expected from the query
		    //Please bare in mind that the correct number of parameters must be given and that only object
		    //from the domain can be used (see eu.paasage.domain.knowledge)
		    for (MyQueryParameter param: parameters) form.field("parameters", param, MediaType.APPLICATION_XML_TYPE);
		    
		    //Running the method with the query parameters posed and the media type 
		    //set and getting back the result
		    ClientResponse response = null;
		    response = r.type(MediaType.MULTIPART_FORM_DATA).post(ClientResponse.class,form);
		    int code = response.getStatus();
		    logger.debug("Status code is: " + code);
		    MyQueryResults qr = null;
		    if (code < 300){
		    	try{
		    		qr = response.getEntity(MyQueryResults.class);
		    		logger.debug("Got query results: " + qr);
		    	}
		    	catch(Exception e){
		    		logger.error("", e);
		    	}
		    }
		    else{
		    	logger.error("Problem: " + response.getEntity(String.class));
		    }
		    logger.debug("... Query running for StatefulKnowledgeSession: " + sessionName + " finished");
		    return qr;
		}
		
		private void deleteSession(String sessionName){
			logger.debug("Deleting StatefulKnowledgeSession with name: " + sessionName + " ...");
			Client c = Client.create();
			c.setFollowRedirects(true);
			HTTPBasicAuthFilter filter = new HTTPBasicAuthFilter(userName,password);
			c.addFilter(filter);
			WebResource r = c.resource(url + "deleteSession");
			
			//Create a MultivaluedMap with one field/parameter
			MultivaluedMap<String, String> params = new MultivaluedMapImpl();
			params.add("sessionName",sessionName);
		    
		    //Running the method with the query parameters posed 
		    //and getting back the result
		    ClientResponse response = null;
		    response = r.queryParams(params).post(ClientResponse.class);
		    logger.debug("Response: " + response.getEntity(String.class));
		    logger.debug("... Deletion of StatefulKnowledgeSession: " + sessionName + " finished");
		}
		
		private void deleteKB(String kbName){
			logger.debug("Deleting KB with name: " + kbName + " ...");
			Client c = Client.create();
			c.setFollowRedirects(true);
			HTTPBasicAuthFilter filter = new HTTPBasicAuthFilter(userName,password);
			c.addFilter(filter);
			WebResource r = c.resource(url + "deleteKB");
			
			//Create a MultivaluedMap with one field/parameter
			MultivaluedMap<String, String> params = new MultivaluedMapImpl();
			params.add("kbName",kbName);
		    
		    //Running the method with the query parameters posed 
		    //and getting back the result
		    ClientResponse response = null;
		    response = r.queryParams(params).post(ClientResponse.class);
		    logger.debug("Response: " + response.getEntity(String.class));
		    logger.debug("... Deletion of KB: " + kbName + " finished");
		}
		
		//This method is required just for printing the results obtained from the execution of a query 
		private static void printQueryResults(MyQueryResults qr){
			if (qr != null){
				List<MyResultRow> results = qr.getResults();
				List<String> identifiers = qr.getIdentifiers();
				logger.debug("Results: " + qr.getResults() + " Identifiers: " + identifiers);
				for (MyResultRow row: results){
					logger.debug("Row with: ");
					for (String s: identifiers){
						MyResultColumn col = row.getResult(s);
						System.out.println("MyResultColumn: " + col.getBasicObject() + " " + col.getCdoID() + " " + col.getDerivedObject() + " ");
						System.out.println("(column = " + s + ",value= " + row.getResult(s) + ") ");
					}
				}
			}
			else logger.debug("Empty/null query results");
		}
		
		private static void loadKBContent(boolean createSession,String fileName){
			JAXBContext context = null;
			try{
				context = JAXBContext.newInstance(eu.paasage.mddb.kb.domain.KBContent.class, eu.paasage.mddb.kb.domain.MySet.class, CDOIDSet.class,eu.paasage.mddb.kb.domain.MyQueryParameter.class, BestApplicationDeployment.class, BestComponentDeployment.class, ApplicationMatch.class, ComponentMatch.class, SuccessfulApplicationDeployment.class,SuccessfulComponentDeployment.class,Wildcard.class);
				String sessionName = "mySession";
				String kbName = "myKB";
				RestClient ec = new RestClient();
				if (createSession){
					ec.createKB(kbName,true);
					ec.createSession(sessionName,kbName);
					ec.addRules(kbName,"queries.drl");
				}
				ObjectInputStream ois = new ObjectInputStream(new FileInputStream(fileName));
				ArrayList al = (ArrayList)ois.readObject();
				/*for (Object o: al){
					System.out.println("Got object: " + o);
				}*/
				//Add the components here
				KBContent cont = new KBContent();
				cont.setElements(al);
				ec.addObjects(sessionName, cont);
			}
			catch(Exception e){
				e.printStackTrace();
			}
		}
		
		private static void storeKBContent(boolean createSession, String fileName){
			JAXBContext context = null;
			try{
				context = JAXBContext.newInstance(eu.paasage.mddb.kb.domain.KBContent.class, eu.paasage.mddb.kb.domain.MySet.class, CDOIDSet.class,eu.paasage.mddb.kb.domain.MyQueryParameter.class, BestApplicationDeployment.class, BestComponentDeployment.class, ApplicationMatch.class, ComponentMatch.class, SuccessfulApplicationDeployment.class,SuccessfulComponentDeployment.class,Wildcard.class);
				String sessionName = "mySession";
				String kbName = "myKB";
				RestClient ec = new RestClient();
				ArrayList al = new ArrayList();
				if (createSession){
					ec.createKB(kbName,true);
					ec.createSession(sessionName,kbName);
					ec.addRules(kbName,"queries.drl");
				}
				//Get the content here
				KBContent mc = ec.getObjects("mySession");
				if (mc != null){
					logger.debug("Unmarshalled KBContent: " + mc);
					Unmarshaller um = context.createUnmarshaller();
					um.setEventHandler(new javax.xml.bind.helpers.DefaultValidationEventHandler());
					for (Object o: mc.getElements()){
						Node node = (Node)o;
						Object o2 = um.unmarshal(node);
						if (o2 instanceof BestApplicationDeployment){
							BestApplicationDeployment bad = (BestApplicationDeployment)o2;
							logger.debug("Got BestApplicationDeployment with: " + bad.getId() + " " + bad.getApplication() + " " + bad.getDeploymentModel() + " " + bad.getMetrics());
							al.add(bad);
						}
						else if (o2 instanceof BestComponentDeployment){
							BestComponentDeployment bad = (BestComponentDeployment)o2;
							logger.debug("Got BestComponentDeployment with: " + bad.getId() + " " + bad.getComponent() + " " + bad.getHostingComponent() + " " + bad.getMetrics());
							al.add(bad);
						}
						else if (o2 instanceof ApplicationMatch){
							ApplicationMatch match = (ApplicationMatch)o2;
							logger.debug("Got ApplicationMatch with: " + match.getFirstApplicationID() + " " + match.getSecondApplicationID() + " " + match.getApplicationMatching());
							al.add(match);
						}
						else if (o2 instanceof ComponentMatch){
							ComponentMatch match = (ComponentMatch)o2;
							logger.debug("Got ComponentMatch with: " + match.getFirstInternalComponentID() + " " + match.getSecondInternalComponentID());
							al.add(match);
						}
						else if (o2 instanceof SuccessfulApplicationDeployment){
							SuccessfulApplicationDeployment sad = (SuccessfulApplicationDeployment)o2;
							logger.debug("Got SuccessfulApplicationDeployment with: " + sad.getApplication() + " " + sad.getDeploymentModel() + " " + sad.getExecutionContexts() + " " + sad.getSlos());
							al.add(sad);
						}
						else if (o2 instanceof SuccessfulComponentDeployment){
							SuccessfulComponentDeployment scd = (SuccessfulComponentDeployment)o2;
							logger.debug("Got SuccessfulComponentDeployment with: " + scd.getComponent() + " " + scd.getHostingComponent() + " " + scd.getExecutionContextID() + " " + scd.getSlos());
							al.add(scd);
						}
					}
				}
				ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream(fileName));
				oos.writeObject(al);
				oos.flush();
				oos.close();
			}
			catch(Exception e){
				e.printStackTrace();
			}
		}
		
		private static String marshall(Object o){
			JAXBContext context = null;
			try{
				context = JAXBContext.newInstance(eu.paasage.mddb.kb.domain.KBContent.class, eu.paasage.mddb.kb.domain.MySet.class, CDOIDSet.class,eu.paasage.mddb.kb.domain.MyQueryParameter.class, BestApplicationDeployment.class, BestComponentDeployment.class, ApplicationMatch.class, ComponentMatch.class, SuccessfulApplicationDeployment.class,SuccessfulComponentDeployment.class,Wildcard.class);
				Marshaller m = context.createMarshaller();
				m.setEventHandler(new javax.xml.bind.helpers.DefaultValidationEventHandler());
				m.setAdapter(new CDOIDAdapter());
				StringWriter sw = new StringWriter();
				m.marshal(o, sw);
				return sw.toString();
			}
			catch(Exception e){
				e.printStackTrace();
			}
			return null;
		}
		
		private static void runQuery(int queryNum){
			JAXBContext context = null;
			try{
				context = JAXBContext.newInstance(eu.paasage.mddb.kb.domain.KBContent.class, eu.paasage.mddb.kb.domain.MySet.class, CDOIDSet.class,eu.paasage.mddb.kb.domain.MyQueryParameter.class, BestApplicationDeployment.class, BestComponentDeployment.class, ApplicationMatch.class, ComponentMatch.class, SuccessfulApplicationDeployment.class,SuccessfulComponentDeployment.class,Wildcard.class);
			}
			catch(Exception e){
				e.printStackTrace();
			}
			String sessionName = "mySession";
			List<MyQueryParameter> params = new ArrayList<MyQueryParameter>();
			RestClient ec = new RestClient();
			switch(queryNum){
				case (0):{
					//applicationMatch
					CDOID id = CDOUtils.getObject("APP1","Application");
					MyQueryParameter param1 = new MyQueryParameter();
					param1.setCdoID(id);
					params.add(param1);
					MyQueryResults results = ec.runQuery("mySession", "applicationMatch", params);
					//System.out.println("Got results: " + results);
					printQueryResults(results);
					break;
				}
				case (1):{
					//componentMatch
					MyQueryParameter param1 = new MyQueryParameter();
					param1.setCdoID(CDOUtils.getObject("IC01","InternalComponent"));
					params.add(param1);
					MyQueryResults results = ec.runQuery("mySession", "componentMatch", params);
					//System.out.println("Got results: " + results);
					printQueryResults(results);
					break;
				}
				case (2):{
					//succAppDep
					CDOID id = CDOUtils.getObject("APP1","Application");
					MyQueryParameter param1 = new MyQueryParameter();
					param1.setCdoID(id);
					params.add(param1);
					param1 = new MyQueryParameter();
					CDOID slo = CDOUtils.getObject("SLO_APP_ET","ServiceLevelObjective");
					System.out.println("slo is: " + slo);
					CDOIDSet slos = new CDOIDSet();
					slos.add(slo);
					param1.setDerivedObject(slos);
					params.add(param1);
					MyQueryResults results = ec.runQuery("mySession", "succAppDep", params);
					//System.out.println("Got results: " + results);
					printQueryResults(results);
					break;
				}
				case (3):{
					//succCompDep
					CDOID id = CDOUtils.getObject("IC01","InternalComponent");
					MyQueryParameter param1 = new MyQueryParameter();
					param1.setCdoID(id);
					params.add(param1);
					param1 = new MyQueryParameter();
					CDOID slo = CDOUtils.getObject("SLO_COMP_ET","ServiceLevelObjective");
					System.out.println("slo is: " + slo);
					CDOIDSet slos = new CDOIDSet();
					slos.add(slo);
					param1.setDerivedObject(slos);
					params.add(param1);
					MyQueryResults results = ec.runQuery("mySession", "succCompDep", params);
					//System.out.println("Got results: " + results);
					printQueryResults(results);
					break;
				}
				case (4):{
					//bestAppDep
					CDOID id = CDOUtils.getObject("APP1","Application");
					MyQueryParameter param1 = new MyQueryParameter();
					param1.setCdoID(id);
					params.add(param1);
					
					param1 = new MyQueryParameter();
					CDOIDSet metrics = new CDOIDSet();
					CDOID metric = CDOUtils.getObject("AVG_APP_ET","Metric");
					System.out.println("Metric is: " + metric);
					metrics.add(metric);
					param1.setDerivedObject(metrics);
					params.add(param1);
					MyQueryResults results = ec.runQuery("mySession", "bestAppDep", params);
					//System.out.println("Got results: " + results);
					printQueryResults(results);
					break;
				}
				case (5):{
					//bestCompDep
					CDOID id = CDOUtils.getObject("IC01","InternalComponent");
					MyQueryParameter param1 = new MyQueryParameter();
					param1.setCdoID(id);
					params.add(param1);
					
					param1 = new MyQueryParameter();
					CDOIDSet metrics = new CDOIDSet();
					CDOID metric = CDOUtils.getObject("AVG_COMP_ET","Metric");
					System.out.println("Metric is: " + metric);
					metrics.add(metric);
					param1.setDerivedObject(metrics);
					params.add(param1);
					MyQueryResults results = ec.runQuery("mySession", "bestCompDep", params);
					//System.out.println("Got results: " + results);
					printQueryResults(results);
					break;
				}
			}
		}
		
		public static void main(String[] args){
			//Load Scenario model in CDO
			if (args.length == 1){
				try{
					boolean small = Boolean.parseBoolean(args[0]);
					//ScenarioCreator.loadScenario(small);
				}
				catch(Exception e){
					e.printStackTrace();
				}
				JAXBContext context = null;
				try{
					context = JAXBContext.newInstance(eu.paasage.mddb.kb.domain.KBContent.class, eu.paasage.mddb.kb.domain.MySet.class, CDOIDSet.class,eu.paasage.mddb.kb.domain.MyQueryParameter.class, BestApplicationDeployment.class, BestComponentDeployment.class, ApplicationMatch.class, ComponentMatch.class, SuccessfulApplicationDeployment.class,SuccessfulComponentDeployment.class,Wildcard.class);
				}
				catch(Exception e){
					e.printStackTrace();
				}
				String sessionName = "mySession";
				String kbName = "myKB";
				//Create the KB client
				RestClient ec = new RestClient();
				//Create a new KB
				ec.createKB(kbName,true);
				//Create a new session for this KB
				ec.createSession(sessionName,kbName);
				ec.fireRules(sessionName);
				//Add rules to the KB mapping to particular queries
				ec.addRules(kbName,"queries.drl");
			}
			else if (args.length == 2){
				try{
					/*ObjectInputStream ois = new ObjectInputStream(new FileInputStream("back"));
					ArrayList al = (ArrayList)ois.readObject();
					KBContent content = new KBContent();
					content.setElements(al);
					System.out.println(marshall(content));
					ois.close();*/
					MyQueryParameter param1 = new MyQueryParameter();
					CDOID slo = CDOUtils.getObject("SLO_COMP_ET","ServiceLevelObjective");
					System.out.println("slo is: " + slo);
					CDOIDSet slos = new CDOIDSet();
					slos.add(slo);
					param1.setDerivedObject(slos);
					System.out.println(marshall(param1));
					CDOID id = CDOUtils.getObject("IC01","InternalComponent");
					param1 = new MyQueryParameter();
					param1.setCdoID(id);
					System.out.println(marshall(param1));
					//loadKBContent(true,"back2");
					//ScenarioCreator.loadScenario(false);
					//storeKBContent(true,"back2");
					/*runQuery(0);
					runQuery(1);
					runQuery(2);
					runQuery(3);
					runQuery(4);
					runQuery(5);*/
				}
				catch(Exception e){
					e.printStackTrace();
				}
			}
			else{
				//This context is needed for the marshalling and unmarshalling of parameters passed as input or obtained back as a result from the API method calls
				JAXBContext context = null;
				try{
					context = JAXBContext.newInstance(eu.paasage.mddb.kb.domain.KBContent.class, eu.paasage.mddb.kb.domain.MySet.class, CDOIDSet.class,eu.paasage.mddb.kb.domain.MyQueryParameter.class, BestApplicationDeployment.class, BestComponentDeployment.class, ApplicationMatch.class, ComponentMatch.class, SuccessfulApplicationDeployment.class,SuccessfulComponentDeployment.class,Wildcard.class);
				}
				catch(Exception e){
					e.printStackTrace();
				}
				String sessionName = "mySession";
				String kbName = "myKB";
				//Create the KB client
				RestClient ec = new RestClient();
				//Create a new KB
				ec.createKB(kbName,true);
				//Create a new session for this KB
				ec.createSession(sessionName,kbName);
				/*//Add a camel model to the session
				ObjectInputStream ois = new ObjectInputStream(new FileInputStream("back"));
				ArrayList al = (ArrayList)ois.readObject();
				//Add the components here
				CamelModel cm = CamelFactory.eINSTANCE.createCamelModel();
				cm.setName("CAMEL MODEL");
				al.add(cm);
				KBContent cont = new KBContent();
				cont.setElements(al);
				ec.addObjects(sessionName, cont);*/
				//Fire all rules for this session
				ec.fireRules(sessionName);
				//Add rules to the KB mapping to particular queries
				ec.addRules(kbName,"queries.drl");
				//Get all objects of derived from the session
				try{
					//String s = ec.getObjects("mySession");
					KBContent mc = ec.getObjects("mySession");
					//if (s != null){
						//s = s.trim();
					if (mc != null){
						logger.debug("Unmarshalled KBContent: " + mc);
						Unmarshaller um = context.createUnmarshaller();
						um.setEventHandler(new javax.xml.bind.helpers.DefaultValidationEventHandler());
						for (Object o: mc.getElements()){
							Node node = (Node)o;
							Object o2 = um.unmarshal(node);
							if (o2 instanceof BestApplicationDeployment){
								BestApplicationDeployment bad = (BestApplicationDeployment)o2;
								logger.debug("Got BestApplicationDeployment with: " + bad.getId() + " " + bad.getApplication() + " " + bad.getDeploymentModel() + " " + bad.getMetrics());
							}
							else if (o2 instanceof BestComponentDeployment){
								BestComponentDeployment bad = (BestComponentDeployment)o2;
								logger.debug("Got BestComponentDeployment with: " + bad.getId() + " " + bad.getComponent() + " " + bad.getHostingComponent() + " " + bad.getMetrics());
							}
							else if (o2 instanceof ApplicationMatch){
								ApplicationMatch match = (ApplicationMatch)o2;
								logger.debug("Got ApplicationMatch with: " + match.getFirstApplicationID() + " " + match.getSecondApplicationID() + " " + match.getApplicationMatching());
							}
							else if (o2 instanceof ComponentMatch){
								ComponentMatch match = (ComponentMatch)o2;
								logger.debug("Got ComponentMatch with: " + match.getFirstInternalComponentID() + " " + match.getSecondInternalComponentID());
							}
							else if (o2 instanceof SuccessfulApplicationDeployment){
								SuccessfulApplicationDeployment sad = (SuccessfulApplicationDeployment)o2;
								logger.debug("Got SuccessfulApplicationDeployment with: " + sad.getApplication() + " " + sad.getDeploymentModel() + " " + sad.getExecutionContexts() + " " + sad.getSlos());
							}
							else if (o2 instanceof SuccessfulComponentDeployment){
								SuccessfulComponentDeployment scd = (SuccessfulComponentDeployment)o2;
								logger.debug("Got SuccessfulComponentDeployment with: " + scd.getComponent() + " " + scd.getHostingComponent() + " " + scd.getExecutionContextID() + " " + scd.getSlos());
							}
						}
					}
				}
				catch(Exception e){
					e.printStackTrace();
				}
				//Issue a particular query to the session, obtain back the results and print them
				List<MyQueryParameter> content = new ArrayList<MyQueryParameter>();
				MyQueryParameter param1 = new MyQueryParameter();
				param1.setCdoID(CDOUtils.getObject("IC11","InternalComponent"));
				//param1.setDerivedObject(new Wildcard());
				content.add(param1);
				/*param1 = new MyQueryParameter();
				CDOID metric = getObject("AVG_COMP_ET","Metric");
				CDOIDSet metrics = new CDOIDSet();
				metrics.add(metric);
				param1.setDerivedObject(metrics);
				content.add(param1);*/
				param1 = new MyQueryParameter();
				CDOID slo = CDOUtils.getObject("SLO_COMP_ET","ServiceLevelObjective");
				CDOIDSet slos = new CDOIDSet();
				slos.add(slo);
				param1.setDerivedObject(slos);
				content.add(param1);
				MyQueryResults qr = ec.runQuery("mySession","succCompDep",content);
				printQueryResults(qr);
				//Delete the session
				ec.deleteSession("mySession");
				//Delete the KB created
				ec.deleteKB(kbName);
			}
		}
	}
