/* Copyright (C) 2015 KYRIAKOS KRITIKOS <kritikos@ics.forth.gr> */

/* This Source Code Form is subject to the terms of the Mozilla Public 
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/ 
 */

package eu.paasage.mddb.kb.client;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.URL;


public class MyIOUtils {
	private static final int BUFFER = 2048;
	private static int id = 1;
	public static void writeInputStreamToFile(InputStream is, String fileName){
		byte[] buffer = new byte[BUFFER];

        try{
        	BufferedInputStream bis = new BufferedInputStream(is);
		    FileOutputStream output = new FileOutputStream(fileName);
		    int count;
            while ((count = bis.read(buffer, 0, BUFFER)) != -1) {
                output.write(buffer, 0, count);
                output.flush();
            }
            output.close();
            bis.close();
        }
        catch(Exception e){
        	e.printStackTrace();
        }
	}
	
	public static File createTempFile(){
		File f = null;
		try{
			f = File.createTempFile("temp", "reader" + (id++));
		}
		catch(Exception e){
			e.printStackTrace();
		}
		return f;
	}
	
	private static File createTempFileFromInputStream(String url){
		File f = null;
		try{
			URL u = new URL(url);
			BufferedReader bis = new BufferedReader(new InputStreamReader(u.openStream()));
			f = File.createTempFile("temp", "reader" + (id++));
			PrintWriter pw = new PrintWriter(new FileWriter(f));
			while (bis.ready()){
				pw.println(bis.readLine());
			}
			bis.close();
			pw.close();
		}
		catch(Exception e){
			e.printStackTrace();
		}
		return f;
	}
	
	private static File createTempFileFromInputStream(InputStream is){
		File f = null;
		try{
			byte[] buffer = new byte[2048];

	        try{
	        	BufferedInputStream bis = new BufferedInputStream(is);
	        	f = File.createTempFile("temp", "reader" + (id++));
			    FileOutputStream output = new FileOutputStream(f);
			    int count;
	            while ((count = bis.read(buffer, 0, 2048)) != -1) {
	                output.write(buffer, 0, count);
	                output.flush();
	            }
	            output.close();
	            bis.close();
	        }
	        catch(Exception e){
	        	e.printStackTrace();
	        }
		}
		catch(Exception e){
			e.printStackTrace();
		}
		return f;
	}
	
	public static String loadQueryFromInputStream(ByteArrayInputStream bis){
		String s = "";
		try{
			byte[] buffer = new byte[2048];
			ByteArrayOutputStream output = new ByteArrayOutputStream();
		    int count;
            while ((count = bis.read(buffer, 0, 2048)) != -1) {
                output.write(buffer, 0, count);
                output.flush();
            }
            output.close();
            s = output.toString();
            bis.close();
		}
		catch(Exception e){
			e.printStackTrace();
		}
		return s;
	}
	
	public static String loadQueryFromFile(File f){
		String s = "";
		try{
			BufferedReader br = new BufferedReader(new FileReader(f));
			String line = null;
			while ((line = br.readLine()) != null){
				s += (line + "\n");
			}
			br.close();
		}
		catch(Exception e){
			e.printStackTrace();
		}
		return s;
	}
	
	public static void printFile(File f) throws Exception{
		BufferedReader br = new BufferedReader(new FileReader(f));
		String line = null;
		while ((line = br.readLine()) != null){
			System.out.println(line);
		}
	}
}
