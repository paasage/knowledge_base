/* Copyright (C) 2015 KYRIAKOS KRITIKOS <kritikos@ics.forth.gr> */

/* This Source Code Form is subject to the terms of the Mozilla Public 
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/ 
 */

package eu.paasage.mddb.kb.client;

import java.util.Date;
import java.util.Random;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.util.EcoreUtil;

import eu.paasage.camel.Application;
import eu.paasage.camel.CamelFactory;
import eu.paasage.camel.CamelModel;
import eu.paasage.camel.CamelPackage;
import eu.paasage.camel.unit.MonetaryUnit;
import eu.paasage.camel.requirement.QuantitativeHardwareRequirement;
import eu.paasage.camel.requirement.RequirementFactory;
import eu.paasage.camel.requirement.RequirementGroup;
import eu.paasage.camel.requirement.RequirementModel;
import eu.paasage.camel.requirement.ServiceLevelObjective;
import eu.paasage.camel.unit.ThroughputUnit;
import eu.paasage.camel.unit.TimeIntervalUnit;
import eu.paasage.camel.unit.UnitFactory;
import eu.paasage.camel.unit.UnitModel;
import eu.paasage.camel.unit.UnitType;
import eu.paasage.camel.deployment.DeploymentFactory;
import eu.paasage.camel.deployment.DeploymentModel;
import eu.paasage.camel.deployment.DeploymentPackage;
import eu.paasage.camel.deployment.Hosting;
import eu.paasage.camel.deployment.HostingInstance;
import eu.paasage.camel.deployment.InternalComponent;
import eu.paasage.camel.deployment.InternalComponentInstance;
import eu.paasage.camel.deployment.ProvidedHost;
import eu.paasage.camel.deployment.ProvidedHostInstance;
import eu.paasage.camel.deployment.RequiredHost;
import eu.paasage.camel.deployment.RequiredHostInstance;
import eu.paasage.camel.deployment.VM;
import eu.paasage.camel.deployment.VMInstance;
import eu.paasage.camel.deployment.VMRequirementSet;
import eu.paasage.camel.execution.ApplicationMeasurement;
import eu.paasage.camel.execution.ExecutionContext;
import eu.paasage.camel.execution.ExecutionFactory;
import eu.paasage.camel.execution.ExecutionModel;
import eu.paasage.camel.execution.InternalComponentMeasurement;
import eu.paasage.camel.execution.SLOAssessment;
import eu.paasage.camel.organisation.CloudProvider;
import eu.paasage.camel.organisation.DataCenter;
import eu.paasage.camel.location.Country;
import eu.paasage.camel.location.LocationFactory;
import eu.paasage.camel.location.LocationModel;
import eu.paasage.camel.organisation.CloudCredentials;
import eu.paasage.camel.organisation.OrganisationFactory;
import eu.paasage.camel.organisation.OrganisationModel;
import eu.paasage.camel.organisation.OrganisationPackage;
import eu.paasage.camel.organisation.PaaSageCredentials;
import eu.paasage.camel.organisation.User;
import eu.paasage.camel.provider.Attribute;
import eu.paasage.camel.provider.AttributeConstraint;
import eu.paasage.camel.provider.FeatCardinality;
import eu.paasage.camel.provider.Feature;
import eu.paasage.camel.provider.Implies;
import eu.paasage.camel.provider.ProviderFactory;
import eu.paasage.camel.provider.ProviderModel;
import eu.paasage.camel.metric.ComparisonOperatorType;
import eu.paasage.camel.LayerType;
import eu.paasage.camel.metric.CompositeMetric;
import eu.paasage.camel.metric.CompositeMetricInstance;
import eu.paasage.camel.metric.MetricContext;
import eu.paasage.camel.metric.MetricFactory;
import eu.paasage.camel.metric.MetricApplicationBinding;
import eu.paasage.camel.metric.MetricComponentBinding;
import eu.paasage.camel.metric.MetricCondition;
import eu.paasage.camel.metric.MetricFormula;
import eu.paasage.camel.metric.MetricFunctionArityType;
import eu.paasage.camel.metric.MetricFunctionType;
import eu.paasage.camel.metric.MetricModel;
import eu.paasage.camel.metric.RawMetric;
import eu.paasage.camel.metric.Property;
import eu.paasage.camel.metric.PropertyType;
import eu.paasage.camel.scalability.ScalabilityFactory;
import eu.paasage.camel.scalability.ScalabilityModel;
import eu.paasage.camel.type.EnumerateValue;
import eu.paasage.camel.type.Enumeration;
import eu.paasage.camel.type.FloatsValue;
import eu.paasage.camel.type.IntegerValue;
import eu.paasage.camel.type.Limit;
import eu.paasage.camel.type.Range;
import eu.paasage.camel.type.StringsValue;
import eu.paasage.camel.type.TypeEnum;
import eu.paasage.camel.type.TypeFactory;
import eu.paasage.camel.type.TypeModel;
import eu.paasage.camel.type.ValueType;
import eu.paasage.mddb.cdo.client.CDOClient;

public class ScenarioCreator {
	
		private static EObject createScenario(boolean small){
			CamelModel camelModel = CamelFactory.eINSTANCE.createCamelModel();
			camelModel.setName("CAMEL MODEL");
			LocationModel lm = LocationFactory.eINSTANCE.createLocationModel();
			lm.setName("LOCATION MODEL");
			camelModel.getLocationModels().add(lm);
			RequirementModel rm = RequirementFactory.eINSTANCE.createRequirementModel();
			rm.setName("REQUIREMENT MODEL");
			camelModel.getRequirementModels().add(rm);
			TypeModel tm = TypeFactory.eINSTANCE.createTypeModel();
			tm.setName("TYPE MODEL");
			camelModel.getTypeModels().add(tm);
			EList<OrganisationModel> orgModels = camelModel.getOrganisationModels();
			EList<Application> applications = camelModel.getApplications();
			EList<ProviderModel> provModels = camelModel.getProviderModels();
			
		////// START definition of Sintef Nova Organisation model
			
			OrganisationModel sintefOrgModel = OrganisationFactory.eINSTANCE.createOrganisationModel();
			sintefOrgModel.setName("SINTEF ORG MODEL");
			EList<DataCenter> sintefDCs = sintefOrgModel.getDataCentres();
			EList<User> sintefUsers = sintefOrgModel.getUsers();
			
			User user1 = OrganisationFactory.eINSTANCE.createUser();
			user1.setEmail("user@sintef.no");
			user1.setFirstName("User1");
			user1.setLastName("User");
			user1.setName("user1");
			PaaSageCredentials pc = OrganisationFactory.eINSTANCE.createPaaSageCredentials();
			pc.setPassword("pwd");
			user1.setPaasageCredentials(pc);
			
			sintefUsers.add(user1);

			CloudProvider sintefNovaProvider = OrganisationFactory.eINSTANCE.createCloudProvider();
			sintefNovaProvider.setEmail("contact@sintef.no");
			sintefNovaProvider.setIaaS(true);
			sintefNovaProvider.setName("Sintef-Nova");
			sintefNovaProvider.setPaaS(true);
			sintefNovaProvider.setPublic(false);
			sintefNovaProvider.setSaaS(false);
			
			sintefOrgModel.setProvider(sintefNovaProvider);
			
			Country osloNovaLocation = LocationFactory.eINSTANCE.createCountry();
			osloNovaLocation.setName("Norway");
			osloNovaLocation.setId("NO");
			
			lm.getCountries().add(osloNovaLocation);
			
			DataCenter sintefDataCenter = OrganisationFactory.eINSTANCE.createDataCenter();
			sintefDataCenter.setCodeName("nova");
			sintefDataCenter.setLocation(osloNovaLocation);
			sintefDataCenter.setName("Sintef Nova Data Centre");
			
			sintefDCs.add(sintefDataCenter);
			
			CloudCredentials user1SintefNovaCredentials = OrganisationFactory.eINSTANCE.createCloudCredentials();
			user1SintefNovaCredentials.setName("CC");
			user1SintefNovaCredentials.setCloudProvider(sintefNovaProvider);
			user1.getCloudCredentials().add(user1SintefNovaCredentials);
			
			orgModels.add(sintefOrgModel);
			
			////// END definition of Sintef Nova Organisation model
			
			///// START definition of Provider Model
			
			ProviderModel providerModel = ProviderFactory.eINSTANCE.createProviderModel();
			providerModel.setName("PROVIDER MODEL");
			
			Feature vmFeature = ProviderFactory.eINSTANCE.createFeature();
			vmFeature.setName("VM");
			FeatCardinality vmCardinality = ProviderFactory.eINSTANCE.createFeatCardinality();
			vmCardinality.setValue(1);
			vmCardinality.setCardinalityMin(1);
			vmCardinality.setCardinalityMax(8);
			vmFeature.setFeatureCardinality(vmCardinality);
			
			providerModel.setRootFeature(vmFeature);
			
			Attribute vmType = ProviderFactory.eINSTANCE.createAttribute();
			vmType.setName("vmType");
			
			Enumeration vmTypes = TypeFactory.eINSTANCE.createEnumeration();
			vmTypes.setName("VMTypes");
			tm.getDataTypes().add(vmTypes);
			
			EnumerateValue smallVm = TypeFactory.eINSTANCE.createEnumerateValue();
			smallVm.setName("SMALL");
			smallVm.setValue(0);
			vmTypes.getValues().add(smallVm);
			
			EnumerateValue mediumVm = TypeFactory.eINSTANCE.createEnumerateValue();
			mediumVm.setName("MEDIUM");
			mediumVm.setValue(1);
			vmTypes.getValues().add(mediumVm);
			
			EnumerateValue largeVm = TypeFactory.eINSTANCE.createEnumerateValue();
			largeVm.setName("LARGE");
			largeVm.setValue(2);
			vmTypes.getValues().add(largeVm);
			
			vmType.setValueType(vmTypes);
			
			vmFeature.getAttributes().add(vmType);
			
			Attribute vmCPU = ProviderFactory.eINSTANCE.createAttribute();
			vmCPU.setName("vmCPU");
			Range vmCPURange = TypeFactory.eINSTANCE.createRange();
			tm.getDataTypes().add(vmCPURange);
			
			vmCPURange.setPrimitiveType(TypeEnum.FLOAT_TYPE);
			
			Limit minCPU = TypeFactory.eINSTANCE.createLimit();
			minCPU.setIncluded(true);
			FloatsValue minCPUValue = TypeFactory.eINSTANCE.createFloatsValue();
			minCPUValue.setValue(1);
			minCPU.setValue(minCPUValue);
			
			Limit maxCPU = TypeFactory.eINSTANCE.createLimit();
			maxCPU.setIncluded(true);
			FloatsValue maxCPUValue = TypeFactory.eINSTANCE.createFloatsValue();
			maxCPUValue.setValue(5);
			maxCPU.setValue(maxCPUValue);
			
			vmCPURange.setLowerLimit(minCPU);
			vmCPURange.setUpperLimit(maxCPU);
			
			vmCPU.setValueType(vmCPURange);
			
			vmFeature.getAttributes().add(vmCPU);
			
			Attribute vmMemory = ProviderFactory.eINSTANCE.createAttribute();
			vmMemory.setName("vmMemory");
			
			Range vmMemoryRange = TypeFactory.eINSTANCE.createRange();
			vmMemoryRange.setName("VMMemoryRange");
			tm.getDataTypes().add(vmMemoryRange);
			
			vmMemoryRange.setPrimitiveType(TypeEnum.INT_TYPE);
			
			Limit minMemory = TypeFactory.eINSTANCE.createLimit();
			minMemory.setIncluded(true);
			IntegerValue minMemoryValue = TypeFactory.eINSTANCE.createIntegerValue();
			minMemoryValue.setValue(2048);
			minMemory.setValue(minMemoryValue);
			
			Limit maxMemory = TypeFactory.eINSTANCE.createLimit();
			maxMemory.setIncluded(true);
			IntegerValue maxMemoryValue = TypeFactory.eINSTANCE.createIntegerValue();
			maxMemoryValue.setValue(16384);
			maxMemory.setValue(maxMemoryValue);
			
			vmMemoryRange.setLowerLimit(minMemory);
			vmMemoryRange.setUpperLimit(maxMemory);
			
			vmMemory.setValueType(vmMemoryRange);
			
			vmFeature.getAttributes().add(vmMemory);
			
			Attribute vmStorage = ProviderFactory.eINSTANCE.createAttribute();
			vmStorage.setName("vmStorage");
			
			Range vmStorageRange = TypeFactory.eINSTANCE.createRange();
			vmStorageRange.setName("VMStorageRange");
			tm.getDataTypes().add(vmStorageRange);
			
			vmStorageRange.setPrimitiveType(TypeEnum.INT_TYPE);
			
			Limit minStorage = TypeFactory.eINSTANCE.createLimit();
			minStorage.setIncluded(true);
			IntegerValue minStorageValue = TypeFactory.eINSTANCE.createIntegerValue();
			minStorageValue.setValue(200);
			minStorage.setValue(minStorageValue);
			
			Limit maxStorage = TypeFactory.eINSTANCE.createLimit();
			maxStorage.setIncluded(true);
			IntegerValue maxStorageValue = TypeFactory.eINSTANCE.createIntegerValue();
			maxStorageValue.setValue(2048);
			maxStorage.setValue(maxStorageValue);
			
			vmStorageRange.setLowerLimit(minStorage);
			vmStorageRange.setUpperLimit(maxStorage);
			
			vmStorage.setValueType(vmStorageRange);
			
			vmFeature.getAttributes().add(vmStorage);
			
			Attribute vmCores = ProviderFactory.eINSTANCE.createAttribute();
			vmCores.setName("vmCores");
			
			Range vmCoresRange = TypeFactory.eINSTANCE.createRange();
			vmCoresRange.setName("VMCoresRange");
			tm.getDataTypes().add(vmCoresRange);
			
			vmCoresRange.setPrimitiveType(TypeEnum.INT_TYPE);
			
			Limit minCores = TypeFactory.eINSTANCE.createLimit();
			minCores.setIncluded(true);
			IntegerValue minCoresValue = TypeFactory.eINSTANCE.createIntegerValue();
			minCoresValue.setValue(1);
			minCores.setValue(minCoresValue);
			
			Limit maxCores = TypeFactory.eINSTANCE.createLimit();
			maxCores.setIncluded(true);
			IntegerValue maxCoresValue = TypeFactory.eINSTANCE.createIntegerValue();
			maxCoresValue.setValue(128);
			maxCores.setValue(maxCoresValue);
			
			vmCoresRange.setLowerLimit(minCores);
			vmCoresRange.setUpperLimit(maxCores);
			
			vmCores.setValueType(vmCoresRange);
			
			vmFeature.getAttributes().add(vmCores);
			
			Implies smallVmConstraint = ProviderFactory.eINSTANCE.createImplies();
			smallVmConstraint.setName("SmallVmConstraint");
			
			smallVmConstraint.setFrom(vmFeature);
			smallVmConstraint.setTo(vmFeature);
			
			
			AttributeConstraint smallVmCPUConstraint = ProviderFactory.eINSTANCE.createAttributeConstraint();
			smallVmCPUConstraint.setFrom(vmType);
			smallVmCPUConstraint.setName("smallvmcpuconstraint");
			StringsValue smallVmCPUConstraintFrom = TypeFactory.eINSTANCE.createStringsValue();
			smallVmCPUConstraintFrom.setValue("SMALL");
			smallVmCPUConstraint.setFromValue(smallVmCPUConstraintFrom);
			
			smallVmCPUConstraint.setTo(vmCPU);
			FloatsValue smallCPUConstraintTo = TypeFactory.eINSTANCE.createFloatsValue();
			smallCPUConstraintTo.setValue(1);
			smallVmCPUConstraint.setToValue(smallCPUConstraintTo);
			
			smallVmConstraint.getAttributeConstraints().add(smallVmCPUConstraint);
			
			
			AttributeConstraint smallVmMemoryConstraint = ProviderFactory.eINSTANCE.createAttributeConstraint();
			smallVmMemoryConstraint.setName("smallvmmemoryconstraint");
			smallVmMemoryConstraint.setFrom(vmType);
			StringsValue smallVmMemoryConstraintFrom = TypeFactory.eINSTANCE.createStringsValue();
			smallVmMemoryConstraintFrom.setValue("SMALL");
			smallVmMemoryConstraint.setFromValue(smallVmMemoryConstraintFrom);
			
			smallVmMemoryConstraint.setTo(vmMemory);
			IntegerValue smallMemoryConstraintTo = TypeFactory.eINSTANCE.createIntegerValue();
			smallMemoryConstraintTo.setValue(2048);
			smallVmMemoryConstraint.setToValue(smallMemoryConstraintTo);
			
			smallVmConstraint.getAttributeConstraints().add(smallVmMemoryConstraint);
			
			
			AttributeConstraint smallVmStorageConstraint = ProviderFactory.eINSTANCE.createAttributeConstraint();
			smallVmStorageConstraint.setName("smallvmstorageconstraint");
			smallVmStorageConstraint.setFrom(vmType);
			StringsValue smallVmStorageConstraintFrom = TypeFactory.eINSTANCE.createStringsValue();
			smallVmStorageConstraintFrom.setValue("SMALL");
			smallVmStorageConstraint.setFromValue(smallVmStorageConstraintFrom);
			
			smallVmStorageConstraint.setTo(vmStorage);
			IntegerValue smallVmStorageConstraintTo = TypeFactory.eINSTANCE.createIntegerValue();
			smallVmStorageConstraintTo.setValue(200);
			smallVmStorageConstraint.setToValue(smallVmStorageConstraintTo);
			
			smallVmConstraint.getAttributeConstraints().add(smallVmStorageConstraint);
			
			
			AttributeConstraint smallVmCoresConstraint = ProviderFactory.eINSTANCE.createAttributeConstraint();
			smallVmCoresConstraint.setFrom(vmType);
			smallVmCoresConstraint.setName("smallvmcoresconstraint");
			StringsValue smallVmCoresConstraintFrom = TypeFactory.eINSTANCE.createStringsValue();
			smallVmCoresConstraintFrom.setValue("SMALL");
			smallVmCoresConstraint.setFromValue(smallVmCoresConstraintFrom);
			
			smallVmCoresConstraint.setTo(vmCores);
			IntegerValue smallVmCoresConstraintTo = TypeFactory.eINSTANCE.createIntegerValue();
			smallVmCoresConstraintTo.setValue(1);
			smallVmCoresConstraint.setToValue(smallVmCoresConstraintTo);
			
			smallVmConstraint.getAttributeConstraints().add(smallVmCoresConstraint);
			
			providerModel.getConstraints().add(smallVmConstraint);

			
			Implies mediumVmConstraint = ProviderFactory.eINSTANCE.createImplies();
			mediumVmConstraint.setName("mediumvmconstraint");
			
			mediumVmConstraint.setFrom(vmFeature);
			mediumVmConstraint.setTo(vmFeature);
			
			
			AttributeConstraint mediumVmCPUConstraint = ProviderFactory.eINSTANCE.createAttributeConstraint();
			mediumVmCPUConstraint.setFrom(vmType);
			mediumVmCPUConstraint.setName("mediumVmCpuConstraint");
			StringsValue mediumVmCPUConstraintFrom = TypeFactory.eINSTANCE.createStringsValue();
			mediumVmCPUConstraintFrom.setValue("MEDIUM");
			mediumVmCPUConstraint.setFromValue(mediumVmCPUConstraintFrom);
			
			mediumVmCPUConstraint.setTo(vmCPU);
			FloatsValue mediumCPUConstraintTo = TypeFactory.eINSTANCE.createFloatsValue();
			mediumCPUConstraintTo.setValue(2);
			mediumVmCPUConstraint.setToValue(mediumCPUConstraintTo);
			
			mediumVmConstraint.getAttributeConstraints().add(mediumVmCPUConstraint);
			
			
			AttributeConstraint mediumVmMemoryConstraint = ProviderFactory.eINSTANCE.createAttributeConstraint();
			mediumVmMemoryConstraint.setFrom(vmType);
			mediumVmMemoryConstraint.setName("mediumVmMemoryConstraint");
			StringsValue mediumVmMemoryConstraintFrom = TypeFactory.eINSTANCE.createStringsValue();
			mediumVmMemoryConstraintFrom.setValue("MEDIUM");
			mediumVmMemoryConstraint.setFromValue(mediumVmMemoryConstraintFrom);
			
			mediumVmMemoryConstraint.setTo(vmMemory);
			IntegerValue mediumMemoryConstraintTo = TypeFactory.eINSTANCE.createIntegerValue();
			mediumMemoryConstraintTo.setValue(4096);
			mediumVmMemoryConstraint.setToValue(mediumMemoryConstraintTo);
			
			mediumVmConstraint.getAttributeConstraints().add(mediumVmMemoryConstraint);
			
			
			AttributeConstraint mediumVmStorageConstraint = ProviderFactory.eINSTANCE.createAttributeConstraint();
			mediumVmStorageConstraint.setFrom(vmType);
			mediumVmStorageConstraint.setName("mediumVmStorageConstraint");
			StringsValue mediumVmStorageConstraintFrom = TypeFactory.eINSTANCE.createStringsValue();
			mediumVmStorageConstraintFrom.setValue("MEDIUM");
			mediumVmStorageConstraint.setFromValue(mediumVmStorageConstraintFrom);
			
			mediumVmStorageConstraint.setTo(vmStorage);
			IntegerValue mediumVmStorageConstraintTo = TypeFactory.eINSTANCE.createIntegerValue();
			mediumVmStorageConstraintTo.setValue(512);
			mediumVmStorageConstraint.setToValue(mediumVmStorageConstraintTo);
			
			mediumVmConstraint.getAttributeConstraints().add(mediumVmStorageConstraint);
			
			
			AttributeConstraint mediumVmCoresConstraint = ProviderFactory.eINSTANCE.createAttributeConstraint();
			mediumVmCoresConstraint.setFrom(vmType);
			mediumVmCoresConstraint.setName("mediumVmCoresConstraint");
			StringsValue mediumVmCoresConstraintFrom = TypeFactory.eINSTANCE.createStringsValue();
			mediumVmCoresConstraintFrom.setValue("MEDIUM");
			mediumVmCoresConstraint.setFromValue(mediumVmCoresConstraintFrom);
			
			mediumVmCoresConstraint.setTo(vmCores);
			IntegerValue mediumVmCoresConstraintTo = TypeFactory.eINSTANCE.createIntegerValue();
			mediumVmCoresConstraintTo.setValue(6);
			mediumVmCoresConstraint.setToValue(mediumVmCoresConstraintTo);
			
			mediumVmConstraint.getAttributeConstraints().add(mediumVmCoresConstraint);
			
			providerModel.getConstraints().add(mediumVmConstraint);
			
			
			Implies largeVmConstraint = ProviderFactory.eINSTANCE.createImplies();
			largeVmConstraint.setName("largevmconstraint");
			largeVmConstraint.setFrom(vmFeature);
			largeVmConstraint.setTo(vmFeature);
			
			
			AttributeConstraint largeVmCPUConstraint = ProviderFactory.eINSTANCE.createAttributeConstraint();
			largeVmCPUConstraint.setFrom(vmType);
			largeVmCPUConstraint.setName("largeVmCpuConstraint");
			StringsValue largeVmCPUConstraintFrom = TypeFactory.eINSTANCE.createStringsValue();
			largeVmCPUConstraintFrom.setValue("LARGE");
			largeVmCPUConstraint.setFromValue(largeVmCPUConstraintFrom);
			
			largeVmCPUConstraint.setTo(vmCPU);
			FloatsValue largeCPUConstraintTo = TypeFactory.eINSTANCE.createFloatsValue();
			largeCPUConstraintTo.setValue((float) 3.2);
			largeVmCPUConstraint.setToValue(largeCPUConstraintTo);
			
			largeVmConstraint.getAttributeConstraints().add(largeVmCPUConstraint);
			
			
			AttributeConstraint largeVmMemoryConstraint = ProviderFactory.eINSTANCE.createAttributeConstraint();
			largeVmMemoryConstraint.setFrom(vmType);
			largeVmMemoryConstraint.setName("largeVmMemoryConstraint");
			StringsValue largeVmMemoryConstraintFrom = TypeFactory.eINSTANCE.createStringsValue();
			largeVmMemoryConstraintFrom.setValue("LARGE");
			largeVmMemoryConstraint.setFromValue(largeVmMemoryConstraintFrom);
			
			largeVmMemoryConstraint.setTo(vmMemory);
			IntegerValue largeMemoryConstraintTo = TypeFactory.eINSTANCE.createIntegerValue();
			largeMemoryConstraintTo.setValue(8192);
			largeVmMemoryConstraint.setToValue(largeMemoryConstraintTo);
			
			largeVmConstraint.getAttributeConstraints().add(largeVmMemoryConstraint);
			
			
			AttributeConstraint largeVmStorageConstraint = ProviderFactory.eINSTANCE.createAttributeConstraint();
			largeVmStorageConstraint.setFrom(vmType);
			largeVmStorageConstraint.setName("largeVmStorageConstraint");
			StringsValue largeVmStorageConstraintFrom = TypeFactory.eINSTANCE.createStringsValue();
			largeVmStorageConstraintFrom.setValue("LARGE");
			largeVmStorageConstraint.setFromValue(largeVmStorageConstraintFrom);
			
			largeVmStorageConstraint.setTo(vmStorage);
			IntegerValue largeVmStorageConstraintTo = TypeFactory.eINSTANCE.createIntegerValue();
			largeVmStorageConstraintTo.setValue(2048);
			largeVmStorageConstraint.setToValue(largeVmStorageConstraintTo);
			
			largeVmConstraint.getAttributeConstraints().add(largeVmStorageConstraint);
			
			
			AttributeConstraint largeVmCoresConstraint = ProviderFactory.eINSTANCE.createAttributeConstraint();
			largeVmCoresConstraint.setFrom(vmType);
			largeVmCoresConstraint.setName("largeVmCoresConstraint");
			StringsValue largeVmCoresConstraintFrom = TypeFactory.eINSTANCE.createStringsValue();
			largeVmCoresConstraintFrom.setValue("LARGE");
			largeVmCoresConstraint.setFromValue(largeVmCoresConstraintFrom);
			
			largeVmCoresConstraint.setTo(vmCores);
			IntegerValue largeVmCoresConstraintTo = TypeFactory.eINSTANCE.createIntegerValue();
			largeVmCoresConstraintTo.setValue(12);
			largeVmCoresConstraint.setToValue(largeVmCoresConstraintTo);
			
			largeVmConstraint.getAttributeConstraints().add(largeVmCoresConstraint);
			
			providerModel.getConstraints().add(largeVmConstraint);
			provModels.add(providerModel);
			
			//Application instances creation
			Application app1 = CamelFactory.eINSTANCE.createApplication();
			app1.setName("APP1");
			app1.setOwner(user1);
			app1.setVersion("1.0");
			applications.add(app1);
			
			Application app2 = CamelFactory.eINSTANCE.createApplication();
			app2.setName("APP2");
			app2.setOwner(user1);
			app2.setVersion("1.0");
			applications.add(app2);
			
			Application app3 = CamelFactory.eINSTANCE.createApplication();
			app3.setName("APP3");
			app3.setOwner(user1);
			app3.setVersion("1.0");
			applications.add(app3);
			
			Application app4 = CamelFactory.eINSTANCE.createApplication();
			app4.setName("APP4");
			app4.setOwner(user1);
			app4.setVersion("1.0");
			applications.add(app4);
			
			Application app5 = CamelFactory.eINSTANCE.createApplication();
			app5.setName("APP5");
			app5.setOwner(user1);
			app5.setVersion("1.0");
			applications.add(app5);
			
			//Create requirement and requirement group for execution contexts to be created
			RequirementGroup rg = RequirementFactory.eINSTANCE.createRequirementGroup();
			rg.setName("RQG1");
			QuantitativeHardwareRequirement qhr = RequirementFactory.eINSTANCE.createQuantitativeHardwareRequirement();
			qhr.setName("QHR");
			rg.getRequirements().add(qhr);
			rm.getRequirements().add(rg);
			rm.getRequirements().add(qhr);
			user1.getRequirementModels().add(rm);
			
			//Create metric templates for the concrete metrics to be created
			ScalabilityModel sm = ScalabilityFactory.eINSTANCE.createScalabilityModel();
			sm.setName("SCALABILITY MODEL");
			camelModel.getScalabilityModels();
			MetricModel mm = MetricFactory.eINSTANCE.createMetricModel();
			mm.setName("METRIC MODEL");
			camelModel.getMetricModels().add(mm);
			UnitModel um = UnitFactory.eINSTANCE.createUnitModel();
			um.setName("UNIT MODEL");
			camelModel.getUnitModels().add(um);
			
			Property et = MetricFactory.eINSTANCE.createProperty();
			et.setName("Execution time");
			et.setType(PropertyType.MEASURABLE);
			mm.getProperties().add(et);
			
			Property th = MetricFactory.eINSTANCE.createProperty();
			th.setName("Throughput");
			th.setType(PropertyType.MEASURABLE);
			mm.getProperties().add(th);
			
			RawMetric rawAppET = MetricFactory.eINSTANCE.createRawMetric();
			rawAppET.setName("RAW_APP_ET");
			rawAppET.setProperty(et);
			rawAppET.setValueDirection((short)0);
			rawAppET.setLayer(LayerType.SAA_S);
			TimeIntervalUnit timeUnit = UnitFactory.eINSTANCE.createTimeIntervalUnit();
			timeUnit.setName("seconds");
			timeUnit.setUnit(UnitType.SECONDS);
			um.getUnits().add(timeUnit);
			rawAppET.setUnit(timeUnit);
			
			CompositeMetric avgAppET = MetricFactory.eINSTANCE.createCompositeMetric();
			avgAppET.setName("AVG_APP_ET");
			avgAppET.setProperty(et);
			avgAppET.setValueDirection((short)0);
			avgAppET.setLayer(LayerType.SAA_S);
			avgAppET.setUnit(timeUnit);
			MetricFormula mf = MetricFactory.eINSTANCE.createMetricFormula();
			mf.setName("MEAN");
			mf.setFunction(MetricFunctionType.MEAN);
			mf.setFunctionArity(MetricFunctionArityType.UNARY);
			mf.getParameters().add(rawAppET);
			avgAppET.setFormula(mf);
			
			RawMetric rawCompET = MetricFactory.eINSTANCE.createRawMetric();
			rawCompET.setName("RAW_COMP_ET");
			rawCompET.setProperty(et);
			rawCompET.setValueDirection((short)0);
			rawCompET.setLayer(LayerType.SAA_S);
			rawCompET.setUnit(timeUnit);
			
			CompositeMetric avgCompET = MetricFactory.eINSTANCE.createCompositeMetric();
			avgCompET.setName("AVG_COMP_ET");
			avgCompET.setProperty(et);
			avgCompET.setValueDirection((short)0);
			avgCompET.setLayer(LayerType.SAA_S);
			avgCompET.setUnit(timeUnit);
			mf = MetricFactory.eINSTANCE.createMetricFormula();
			mf.setName("MEAN");
			mf.setFunction(MetricFunctionType.MEAN);
			mf.setFunctionArity(MetricFunctionArityType.UNARY);
			mf.getParameters().add(rawCompET);
			avgCompET.setFormula(mf);
			
			RawMetric rawAppTH = MetricFactory.eINSTANCE.createRawMetric();
			rawAppTH.setName("RAW_APP_TH");
			rawAppTH.setProperty(th);
			rawAppTH.setValueDirection((short)1);
			rawAppTH.setLayer(LayerType.SAA_S);
			ThroughputUnit thUnit = UnitFactory.eINSTANCE.createThroughputUnit();
			thUnit.setName("reqs/second");
			timeUnit.setUnit(UnitType.REQUESTS_PER_SECOND);
			um.getUnits().add(thUnit);
			rawAppTH.setUnit(thUnit);
			
			CompositeMetric avgAppTH = MetricFactory.eINSTANCE.createCompositeMetric();
			avgAppTH.setName("AVG_APP_TH");
			avgAppTH.setProperty(th);
			avgAppTH.setValueDirection((short)1);
			avgAppTH.setLayer(LayerType.SAA_S);
			avgAppTH.setUnit(thUnit);
			mf = MetricFactory.eINSTANCE.createMetricFormula();
			mf.setName("MEAN");
			mf.setFunction(MetricFunctionType.MEAN);
			mf.setFunctionArity(MetricFunctionArityType.UNARY);
			mf.getParameters().add(rawAppTH);
			avgAppTH.setFormula(mf);
			
			RawMetric rawCompTH = MetricFactory.eINSTANCE.createRawMetric();
			rawCompTH.setName("RAW_COMP_TH");
			rawCompTH.setProperty(th);
			rawCompTH.setValueDirection((short)1);
			rawCompTH.setLayer(LayerType.SAA_S);
			rawCompTH.setUnit(thUnit);
			
			CompositeMetric avgCompTH = MetricFactory.eINSTANCE.createCompositeMetric();
			avgCompTH.setName("AVG_COMP_TH");
			avgCompTH.setProperty(th);
			avgCompTH.setValueDirection((short)1);
			avgCompTH.setLayer(LayerType.SAA_S);
			avgCompTH.setUnit(thUnit);
			mf = MetricFactory.eINSTANCE.createMetricFormula();
			mf.setName("MEAN");
			mf.setFunction(MetricFunctionType.MEAN);
			mf.setFunctionArity(MetricFunctionArityType.UNARY);
			mf.getParameters().add(rawCompTH);
			avgCompTH.setFormula(mf);
			
			mm.getMetrics().add(rawCompET);
			mm.getMetrics().add(rawCompTH);
			mm.getMetrics().add(rawAppET);
			mm.getMetrics().add(rawAppTH);
			mm.getMetrics().add(avgAppET);
			mm.getMetrics().add(avgAppTH);
			mm.getMetrics().add(avgCompET);
			mm.getMetrics().add(avgCompTH);
			
			MetricCondition mc = MetricFactory.eINSTANCE.createMetricCondition();
			mc.setName("MC_APP_ET");
			mc.setThreshold(3000);
			mc.setComparisonOperator(ComparisonOperatorType.LESS_EQUAL_THAN);
			MetricContext mcc = MetricFactory.eINSTANCE.createCompositeMetricContext();
			mcc.setName("MCC_APP_ET");
			mcc.setMetric(avgAppET);
			mm.getContexts().add(mcc);
			mm.getConditions().add(mc);
			mc.setMetricContext(mcc);
			ServiceLevelObjective slo1 = RequirementFactory.eINSTANCE.createServiceLevelObjective();
			slo1.setName("SLO_APP_ET");
			slo1.setCustomServiceLevel(mc);
			rm.getRequirements().add(slo1);
			
			mc = MetricFactory.eINSTANCE.createMetricCondition();
			mc.setName("MC_COMP_ET");
			mc.setThreshold(1000);
			mc.setComparisonOperator(ComparisonOperatorType.LESS_EQUAL_THAN);
			mcc = MetricFactory.eINSTANCE.createCompositeMetricContext();
			mcc.setName("MCC_COMP_ET");
			mcc.setMetric(avgCompET);
			mm.getContexts().add(mcc);
			mm.getConditions().add(mc);
			mc.setMetricContext(mcc);
			slo1 = RequirementFactory.eINSTANCE.createServiceLevelObjective();
			slo1.setName("SLO_COMP_ET");
			slo1.setCustomServiceLevel(mc);
			rm.getRequirements().add(slo1);
			
			mc = MetricFactory.eINSTANCE.createMetricCondition();
			mc.setName("MC_APP_TH");
			mc.setThreshold(50000);
			mc.setComparisonOperator(ComparisonOperatorType.GREATER_EQUAL_THAN);
			mcc = MetricFactory.eINSTANCE.createCompositeMetricContext();
			mcc.setName("MCC_APP_TH");
			mcc.setMetric(avgAppTH);
			mm.getContexts().add(mcc);
			mm.getConditions().add(mc);
			mc.setMetricContext(mcc);
			slo1 = RequirementFactory.eINSTANCE.createServiceLevelObjective();
			slo1.setName("SLO_APP_TH");
			slo1.setCustomServiceLevel(mc);
			rm.getRequirements().add(slo1);
			
			mc = MetricFactory.eINSTANCE.createMetricCondition();
			mc.setName("MC_COMP_TH");
			mc.setThreshold(50000);
			mc.setComparisonOperator(ComparisonOperatorType.GREATER_EQUAL_THAN);
			mcc = MetricFactory.eINSTANCE.createCompositeMetricContext();
			mcc.setName("MCC_COMP_TH");
			mcc.setMetric(avgCompTH);
			mm.getContexts().add(mcc);
			mm.getConditions().add(mc);
			mc.setMetricContext(mcc);
			slo1 = RequirementFactory.eINSTANCE.createServiceLevelObjective();
			slo1.setName("SLO_Comp_TH");
			slo1.setCustomServiceLevel(mc);
			rm.getRequirements().add(slo1);
			
			int num = 4;
			if (!small) num = 100;
			
			createDeploymentExecutionModels(camelModel,app1,sintefNovaProvider,num,true);
			createDeploymentExecutionModels(camelModel,app2,sintefNovaProvider,num,true);
			createDeploymentExecutionModels(camelModel,app3,sintefNovaProvider,num,true);
			createDeploymentExecutionModels(camelModel,app4,sintefNovaProvider,num,false);
			createDeploymentExecutionModels(camelModel,app5,sintefNovaProvider,num,false);
			
			return camelModel;
		}
		
		private static void createDeploymentExecutionModels(CamelModel cm, Application app, CloudProvider provider, int number, boolean success){
			MonetaryUnit cu = UnitFactory.eINSTANCE.createMonetaryUnit();
			cu.setName("EUROS");
			cu.setUnit(UnitType.EUROS);
			UnitModel um = cm.getUnitModels().get(0);
			um.getUnits().add(cu);
			RequirementModel rm = cm.getRequirementModels().get(0);
			
			for (int i = 0; i < number; i++){
				//InternalComponents creation
				int mod = (i % 2);
				String id = "" + mod;
				boolean middle = (mod == 1);
				
				InternalComponent ic1 = DeploymentFactory.eINSTANCE.createInternalComponent();
				ic1.setName("IC" + id + "1");
				RequiredHost rh1 = DeploymentFactory.eINSTANCE.createRequiredHost();
				rh1.setName("RH1");
				ic1.setRequiredHost(rh1);
				InternalComponent ic2 = DeploymentFactory.eINSTANCE.createInternalComponent();
				ic2.setName("IC" + id + "2");
				RequiredHost rh2 = DeploymentFactory.eINSTANCE.createRequiredHost();
				rh2.setName("RH2");
				ic2.setRequiredHost(rh2);
				InternalComponent ic3 = DeploymentFactory.eINSTANCE.createInternalComponent();
				ic3.setName("IC" + id + "3");
				RequiredHost rh3 = DeploymentFactory.eINSTANCE.createRequiredHost();
				rh3.setName("RH3");
				ic3.setRequiredHost(rh3);
				ProvidedHost ph3 = DeploymentFactory.eINSTANCE.createProvidedHost();
				ph3.setName("PH3");
				ic3.getProvidedHosts().add(ph3);
				
				//DeploymentModel partial creation
				DeploymentModel app1DepModel = DeploymentFactory.eINSTANCE.createDeploymentModel();
				app1DepModel.setName("App" + i + "DM");
				
				//VMs creation
				VM vm1 = DeploymentFactory.eINSTANCE.createVM();
				vm1.setName("VM" + id + "1");
				VMRequirementSet vrs = DeploymentFactory.eINSTANCE.createVMRequirementSet();
				vm1.setVmRequirementSet(vrs);
				vrs.setName("VRS1");
				app1DepModel.getVmRequirementSets().add(vrs);
				QuantitativeHardwareRequirement req = RequirementFactory.eINSTANCE.createQuantitativeHardwareRequirement();
				rm.getRequirements().add(req);
				vrs.setQuantitativeHardwareRequirement(req);
				req.setName("HWREQ");
				req.setMinCores(1);
				req.setMaxCores(4);
				req.setMinRAM(2048);
				req.setMaxRAM(8096);
				req.setMinStorage(200);
				req.setMaxStorage(1024);
				req.setName("VM1");
				ProvidedHost vmph1 = DeploymentFactory.eINSTANCE.createProvidedHost();
				vmph1.setName("VMPH1");
				vm1.getProvidedHosts().add(vmph1);
				VM vm2 = DeploymentFactory.eINSTANCE.createVM();
				vrs = DeploymentFactory.eINSTANCE.createVMRequirementSet();
				vrs.setName("VRS2");
				app1DepModel.getVmRequirementSets().add(vrs);
				vm2.setVmRequirementSet(vrs);
				vm2.setName("VM" + id + "2");
				req = RequirementFactory.eINSTANCE.createQuantitativeHardwareRequirement();
				rm.getRequirements().add(req);
				req.setMinCores(1);
				req.setMaxCores(4);
				req.setMinRAM(2048);
				req.setMaxRAM(8096);
				req.setMinStorage(200);
				req.setMaxStorage(1024);
				req.setName("VM1");
				vrs.setQuantitativeHardwareRequirement(req);
				ProvidedHost vmph2 = DeploymentFactory.eINSTANCE.createProvidedHost();
				vmph2.setName("VMPH2");
				vm2.getProvidedHosts().add(vmph2);
				
				Hosting h1 = DeploymentFactory.eINSTANCE.createHosting();
				h1.setName("H1");
				h1.setProvidedHost(vmph1);
				h1.setRequiredHost(rh1);
				app1DepModel.getHostings().add(h1);
				Hosting h2 = DeploymentFactory.eINSTANCE.createHosting();
				h2.setName("H2");
				h2.setProvidedHost(ph3);
				h2.setRequiredHost(rh2);
				app1DepModel.getHostings().add(h2);
				Hosting h3 = DeploymentFactory.eINSTANCE.createHosting();
				h3.setName("H3");
				h3.setProvidedHost(vmph2);
				h3.setRequiredHost(rh3);
				app1DepModel.getHostings().add(h3);
				
				
				//InternalComponentInstances creation
				InternalComponentInstance ici1 = DeploymentFactory.eINSTANCE.createInternalComponentInstance();
				ici1.setName("ICI1");
				ici1.setType(ic1);
				RequiredHostInstance rhi1 = DeploymentFactory.eINSTANCE.createRequiredHostInstance();
				rhi1.setName("RHI1");
				rhi1.setType(rh1);
				ici1.setRequiredHostInstance(rhi1);
				InternalComponentInstance ici2 = DeploymentFactory.eINSTANCE.createInternalComponentInstance();
				ici2.setName("ICI2");
				ici2.setType(ic2);
				RequiredHostInstance rhi2 = DeploymentFactory.eINSTANCE.createRequiredHostInstance();
				rhi2.setName("RHI2");
				rhi2.setType(rh2);
				ici2.setRequiredHostInstance(rhi2);
				InternalComponentInstance ici3 = DeploymentFactory.eINSTANCE.createInternalComponentInstance();
				ici3.setName("ICI3");
				ici3.setType(ic3);
				RequiredHostInstance rhi3 = DeploymentFactory.eINSTANCE.createRequiredHostInstance();
				rhi3.setName("RHI3");
				rhi3.setType(rh3);
				ici3.setRequiredHostInstance(rhi3);
				ProvidedHostInstance phi3 = DeploymentFactory.eINSTANCE.createProvidedHostInstance();
				phi3.setName("PHI3");
				phi3.setType(ph3);
				ici3.getProvidedHostInstances().add(phi3);
				
				//VM instances creation !!!add info objects
				ProviderModel pm = cm.getProviderModels().get(0);
				Attribute vmType = pm.getRootFeature().getAttributes().get(0);
				ValueType vt = vmType.getValueType();
				EnumerateValue low = null, medium = null;
				if (vt instanceof Enumeration){
					Enumeration en = (Enumeration)vt;
					EList<EnumerateValue> vals = en.getValues();
					low = vals.get(0);
					medium = vals.get(1);
				}
				VMInstance vmi1 = DeploymentFactory.eINSTANCE.createVMInstance();
				vmi1.setType(vm1);
				vmi1.setName("VMI1");
				vmi1.setVmType(vmType);
				if (!middle) vmi1.setVmTypeValue(low);
				else vmi1.setVmTypeValue(medium);
				ProvidedHostInstance vmiphi1 = DeploymentFactory.eINSTANCE.createProvidedHostInstance();
				vmiphi1.setName("VMIPH1");
				vmiphi1.setType(vmph1);
				vmi1.getProvidedHostInstances().add(vmiphi1);
				//vmi1.setHasInfo(info1);
				VMInstance vmi2 = DeploymentFactory.eINSTANCE.createVMInstance();
				vmi2.setType(vm2);
				vmi2.setName("VMI2");
				vmi2.setVmType(vmType);
				if (!middle) vmi2.setVmTypeValue(medium);
				else vmi2.setVmTypeValue(low);
				ProvidedHostInstance vmiphi2 = DeploymentFactory.eINSTANCE.createProvidedHostInstance();
				vmiphi2.setName("VMIPH2");
				vmiphi2.setType(vmph2);
				vmi2.getProvidedHostInstances().add(vmiphi2);
				//vmi2.setHasInfo(info2);
				
				//HostingInstances creation
				HostingInstance hi1 = DeploymentFactory.eINSTANCE.createHostingInstance();
				hi1.setName("HI1");
				hi1.setProvidedHostInstance(vmiphi1);
				hi1.setRequiredHostInstance(rhi1);
				hi1.setType(h1);
				HostingInstance hi2 = DeploymentFactory.eINSTANCE.createHostingInstance();
				hi2.setName("HI2");
				hi2.setProvidedHostInstance(phi3);
				hi2.setRequiredHostInstance(rhi2);
				hi2.setType(h2);
				HostingInstance hi3 = DeploymentFactory.eINSTANCE.createHostingInstance();
				hi3.setName("HI3");
				hi3.setProvidedHostInstance(vmiphi2);
				hi3.setRequiredHostInstance(rhi3);
				hi3.setType(h3);

				
				//Application Deployment Models creation
				app1DepModel.getInternalComponents().add(ic1);
				app1DepModel.getInternalComponents().add(ic2);
				app1DepModel.getInternalComponents().add(ic3);
				app1DepModel.getInternalComponentInstances().add(ici1);
				app1DepModel.getInternalComponentInstances().add(ici2);
				app1DepModel.getInternalComponentInstances().add(ici3);
				app1DepModel.getVms().add(vm1);
				app1DepModel.getVms().add(vm2);
				app1DepModel.getVmInstances().add(vmi1);
				app1DepModel.getVmInstances().add(vmi2);
				app1DepModel.getHostingInstances().add(hi1);
				app1DepModel.getHostingInstances().add(hi2);
				app1DepModel.getHostingInstances().add(hi3);
				cm.getDeploymentModels().add(app1DepModel);
				app.getDeploymentModels().add(app1DepModel);
				
				//Execution Context creation
				ExecutionModel em = ExecutionFactory.eINSTANCE.createExecutionModel();
				em.setName("EXECUTION MODEL");
				ExecutionContext ec = ExecutionFactory.eINSTANCE.createExecutionContext();
				ec.setName(EcoreUtil.generateUUID());
				ec.setApplication(app);
				ec.setCostUnit(cu);
				ec.setDeploymentModel(app1DepModel);
				ec.setRequirementGroup((RequirementGroup)rm.getRequirements().get(0));
				em.getExecutionContexts().add(ec);
				
				//Create ScalabilityModel to include instances of metric templates
				MetricModel mm = MetricFactory.eINSTANCE.createMetricModel();
				mm.setName("METRIC MODEL");
				
				CompositeMetricInstance avgAppET = MetricFactory.eINSTANCE.createCompositeMetricInstance();
				avgAppET.setName(EcoreUtil.generateUUID());
				avgAppET.setMetric(cm.getMetricModels().get(0).getMetrics().get(4));
				MetricApplicationBinding maib = MetricFactory.eINSTANCE.createMetricApplicationBinding();
				maib.setExecutionContext(ec);
				maib.setName("MAIB");
				avgAppET.setObjectBinding(maib);
				mm.getMetricInstances().add(avgAppET);
				mm.getBindings().add(maib);
				
				CompositeMetricInstance avgAppTH = MetricFactory.eINSTANCE.createCompositeMetricInstance();
				avgAppTH.setName(EcoreUtil.generateUUID());
				avgAppTH.setMetric(cm.getMetricModels().get(0).getMetrics().get(5));
				avgAppTH.setObjectBinding(maib);
				mm.getMetricInstances().add(avgAppTH);
				
				CompositeMetricInstance avgCompET1 = MetricFactory.eINSTANCE.createCompositeMetricInstance();
				avgCompET1.setName(EcoreUtil.generateUUID());
				avgCompET1.setMetric(cm.getMetricModels().get(0).getMetrics().get(6));
				MetricComponentBinding mcib = MetricFactory.eINSTANCE.createMetricComponentBinding();
				mcib.setName("MCIB");
				mcib.setExecutionContext(ec);
				mcib.setComponentInstance(ici1);
				avgCompET1.setObjectBinding(mcib);
				mm.getMetricInstances().add(avgCompET1);
				mm.getBindings().add(mcib);
				
				CompositeMetricInstance avgCompTH1 = MetricFactory.eINSTANCE.createCompositeMetricInstance();
				avgCompTH1.setName(EcoreUtil.generateUUID());
				avgCompTH1.setMetric(cm.getMetricModels().get(0).getMetrics().get(7));
				avgCompTH1.setObjectBinding(mcib);
				mm.getMetricInstances().add(avgCompTH1);
				
				CompositeMetricInstance avgCompET2 = MetricFactory.eINSTANCE.createCompositeMetricInstance();
				avgCompET2.setName(EcoreUtil.generateUUID());
				avgCompET2.setMetric(cm.getMetricModels().get(0).getMetrics().get(6));
				MetricComponentBinding mcib2 = MetricFactory.eINSTANCE.createMetricComponentBinding();
				mcib2.setExecutionContext(ec);
				mcib2.setComponentInstance(ici2);
				mcib2.setName("MCIB2");
				avgCompET2.setObjectBinding(mcib2);
				mm.getMetricInstances().add(avgCompET2);
				mm.getBindings().add(mcib2);
				
				CompositeMetricInstance avgCompTH2 = MetricFactory.eINSTANCE.createCompositeMetricInstance();
				avgCompTH2.setName(EcoreUtil.generateUUID());
				avgCompTH2.setMetric(cm.getMetricModels().get(0).getMetrics().get(7));
				avgCompTH2.setObjectBinding(mcib2);
				mm.getMetricInstances().add(avgCompTH2);
				
				CompositeMetricInstance avgCompET3 = MetricFactory.eINSTANCE.createCompositeMetricInstance();
				avgCompET3.setName(EcoreUtil.generateUUID());
				avgCompET3.setMetric(cm.getMetricModels().get(0).getMetrics().get(6));
				MetricComponentBinding mcib3 = MetricFactory.eINSTANCE.createMetricComponentBinding();
				mcib3.setExecutionContext(ec);
				mcib3.setComponentInstance(ici3);
				mcib3.setName("MCIB3");
				avgCompET3.setObjectBinding(mcib3);
				mm.getMetricInstances().add(avgCompET3);
				mm.getBindings().add(mcib3);
				
				CompositeMetricInstance avgCompTH3 = MetricFactory.eINSTANCE.createCompositeMetricInstance();
				avgCompTH3.setName(EcoreUtil.generateUUID());
				avgCompTH3.setMetric(cm.getMetricModels().get(0).getMetrics().get(7));
				avgCompTH3.setObjectBinding(mcib3);
				mm.getMetricInstances().add(avgCompTH3);
				
				Random random = new Random();
				
				InternalComponentMeasurement rm1 = ExecutionFactory.eINSTANCE.createInternalComponentMeasurement();
				rm1.setName("RM1");
				rm1.setExecutionContext(ec);
				rm1.setMetricInstance(avgCompET1);
				if (success || !middle) rm1.setValue(random.nextInt(1000));
				else rm1.setValue(random.nextInt(1000) + 1001);
				rm1.setMeasurementTime(new Date());
				rm1.setInternalComponentInstance(ici1);
				em.getMeasurements().add(rm1);
				
				SLOAssessment slo = ExecutionFactory.eINSTANCE.createSLOAssessment();
				slo.setName("SLO_Comp_ET");
				slo.setAssessment(success || !middle);
				slo.setAssessmentTime(new Date());
				slo.setExecutionContext(ec);
				slo.setMeasurement(rm1);
				slo.setSlo((ServiceLevelObjective)(rm.getRequirements().get(3)));
				em.getSloAssessessments().add(slo);
				
				InternalComponentMeasurement rm2 = ExecutionFactory.eINSTANCE.createInternalComponentMeasurement();
				rm2.setExecutionContext(ec);
				rm2.setName("RM2");
				rm2.setMetricInstance(avgCompET2);
				if (success || !middle) rm2.setValue(random.nextInt(1000));
				else rm2.setValue(random.nextInt(1000) + 1001);
				rm2.setMeasurementTime(new Date());
				rm2.setInternalComponentInstance(ici2);
				em.getMeasurements().add(rm2);
				
				slo = ExecutionFactory.eINSTANCE.createSLOAssessment();
				slo.setName("SLO_Comp_ET");
				slo.setAssessment(success || !middle);
				slo.setAssessmentTime(new Date());
				slo.setExecutionContext(ec);
				slo.setMeasurement(rm2);
				slo.setSlo((ServiceLevelObjective)(rm.getRequirements().get(3)));
				em.getSloAssessessments().add(slo);
				
				InternalComponentMeasurement rm3 = ExecutionFactory.eINSTANCE.createInternalComponentMeasurement();
				rm3.setName("RM3");
				rm3.setExecutionContext(ec);
				rm3.setMetricInstance(avgCompET3);
				if (success || !middle) rm3.setValue(random.nextInt(1000));
				else rm3.setValue(random.nextInt(1000) + 1001);
				rm3.setMeasurementTime(new Date());
				rm3.setInternalComponentInstance(ici3);
				em.getMeasurements().add(rm3);
				
				slo = ExecutionFactory.eINSTANCE.createSLOAssessment();
				slo.setName("SLO_Comp_ET");
				slo.setAssessment(success || !middle);
				slo.setAssessmentTime(new Date());
				slo.setExecutionContext(ec);
				slo.setMeasurement(rm3);
				slo.setSlo((ServiceLevelObjective)(rm.getRequirements().get(3)));
				em.getSloAssessessments().add(slo);
				
				ApplicationMeasurement am = ExecutionFactory.eINSTANCE.createApplicationMeasurement();
				am.setName("AM1");
				am.setExecutionContext(ec);
				am.setMetricInstance(avgAppET);
				am.setValue(rm1.getValue() + rm2.getValue() + rm3.getValue());
				am.setMeasurementTime(new Date());
				am.setApplication(app);
				em.getMeasurements().add(am);
				
				slo = ExecutionFactory.eINSTANCE.createSLOAssessment();
				slo.setName("SLO_App_ET");
				slo.setAssessment(success || !middle);
				slo.setAssessmentTime(new Date());
				slo.setExecutionContext(ec);
				slo.setMeasurement(am);
				slo.setSlo((ServiceLevelObjective)(rm.getRequirements().get(2)));
				em.getSloAssessessments().add(slo);
				
				rm1 = ExecutionFactory.eINSTANCE.createInternalComponentMeasurement();
				rm1.setName("RM1");
				rm1.setExecutionContext(ec);
				rm1.setMetricInstance(avgCompTH1);
				if (success || !middle) rm1.setValue(random.nextInt(50000) + 50000);
				else rm1.setValue(random.nextInt(50000));
				rm1.setMeasurementTime(new Date());
				rm1.setInternalComponentInstance(ici1);
				em.getMeasurements().add(rm1);
				
				slo = ExecutionFactory.eINSTANCE.createSLOAssessment();
				slo.setName("SLO_Comp_TH");
				slo.setAssessment(success || !middle);
				slo.setAssessmentTime(new Date());
				slo.setExecutionContext(ec);
				slo.setMeasurement(rm1);
				slo.setSlo((ServiceLevelObjective)(rm.getRequirements().get(5)));
				em.getSloAssessessments().add(slo);
				
				rm2 = ExecutionFactory.eINSTANCE.createInternalComponentMeasurement();
				rm2.setName("RM2");
				rm2.setExecutionContext(ec);
				rm2.setMetricInstance(avgCompTH2);
				if (success || !middle) rm2.setValue(random.nextInt(50000) + 50000);
				else rm2.setValue(random.nextInt(50000));
				rm2.setMeasurementTime(new Date());
				rm2.setInternalComponentInstance(ici2);
				em.getMeasurements().add(rm2);
				
				slo = ExecutionFactory.eINSTANCE.createSLOAssessment();
				slo.setName("SLO_Comp_TH");
				slo.setAssessment(success || !middle);
				slo.setAssessmentTime(new Date());
				slo.setExecutionContext(ec);
				slo.setMeasurement(rm2);
				slo.setSlo((ServiceLevelObjective)(rm.getRequirements().get(5)));
				em.getSloAssessessments().add(slo);
				
				rm3 = ExecutionFactory.eINSTANCE.createInternalComponentMeasurement();
				rm3.setName("RM3");
				rm3.setExecutionContext(ec);
				rm3.setMetricInstance(avgCompTH3);
				if (success || !middle) rm3.setValue(random.nextInt(50000) + 50000);
				else rm3.setValue(random.nextInt(50000));
				rm3.setMeasurementTime(new Date());
				rm3.setInternalComponentInstance(ici3);
				em.getMeasurements().add(rm3);
				
				slo = ExecutionFactory.eINSTANCE.createSLOAssessment();
				slo.setName("SLO_Comp_TH");
				slo.setAssessment(success || !middle);
				slo.setAssessmentTime(new Date());
				slo.setExecutionContext(ec);
				slo.setMeasurement(rm3);
				slo.setSlo((ServiceLevelObjective)(rm.getRequirements().get(5)));
				em.getSloAssessessments().add(slo);
				
				am = ExecutionFactory.eINSTANCE.createApplicationMeasurement();
				am.setName("AM1");
				am.setExecutionContext(ec);
				am.setMetricInstance(avgAppTH);
				am.setValue(Math.min(rm1.getValue(), Math.min(rm2.getValue(), rm3.getValue())));
				am.setMeasurementTime(new Date());
				am.setApplication(app);
				em.getMeasurements().add(am);
				
				slo = ExecutionFactory.eINSTANCE.createSLOAssessment();
				slo.setName("SLO_App_TH");
				slo.setAssessment(success || !middle);
				slo.setAssessmentTime(new Date());
				slo.setExecutionContext(ec);
				slo.setMeasurement(am);
				slo.setSlo((ServiceLevelObjective)(rm.getRequirements().get(4)));
				em.getSloAssessessments().add(slo);
				
				cm.getMetricModels().add(mm);
				cm.getExecutionModels().add(em);
			}
		}
		
						
		public static void loadScenario(boolean small) throws Exception{
			CDOClient cl = new CDOClient();
			EObject camelScenario = createScenario(small);
			cl.storeModel(camelScenario, "Camel_Scenario",false);
			cl.closeSession();
		}
}
