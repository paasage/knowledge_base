/* Copyright (C) 2015 KYRIAKOS KRITIKOS <kritikos@ics.forth.gr> */

/* This Source Code Form is subject to the terms of the Mozilla Public 
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/ 
 */

package eu.paasage.mddb.kb.domain;

import java.util.Set;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;

import org.eclipse.emf.cdo.common.id.CDOID;

@XmlRootElement
public class SuccessfulApplicationDeployment implements java.io.Serializable{
	private static final long serialVersionUID = 1118516919244655300L;
	private static int idGen = 1;
	private int id;
	private CDOID application;
	private Set<CDOID> slos;
	private CDOID deploymentModel;
	private Set<CDOID> executionContexts;
	//private Set<ApplMonitor> applMonitors;
	
	public SuccessfulApplicationDeployment(){
		id = idGen++;
	}
	
	public SuccessfulApplicationDeployment(int id){
		this.id = id;
	}
	
	public SuccessfulApplicationDeployment(int id, CDOID application, Set<CDOID> slos, CDOID dm){
		this.id = id;
		this.application = application;
		this.slos = slos;
		this.deploymentModel = dm;
	}
	
	@XmlAttribute
	public int getId(){
		return id;
	}
	
	public void setId(int id){
		this.id = id;
	}
	
	@XmlAttribute
	public CDOID getApplication(){
		return application;
	}
	
	public void setApplication(CDOID application){
		this.application = application;
	}
	
	public Set<CDOID> getSlos(){
		return slos;
	}
	
	public void setSlos(Set<CDOID> slos){
		this.slos = slos;
	}
	
	public Set<CDOID> getExecutionContexts(){
		return executionContexts;
	}
	
	public void setExecutionContexts(Set<CDOID> executionContexts){
		this.executionContexts = executionContexts;
	}
	
	@XmlAttribute
	public CDOID getDeploymentModel(){
		return deploymentModel;
	}
	
	public void setDeploymentModel(CDOID deploymentModel){
		this.deploymentModel = deploymentModel;
	}
	
	public boolean equals(Object o){
		if (o instanceof SuccessfulApplicationDeployment){
			SuccessfulApplicationDeployment bda = (SuccessfulApplicationDeployment)o;
			if (bda.getApplication().equals(application) && bda.getSlos().equals(slos) && bda.getDeploymentModel().equals(deploymentModel)) return true;
		}
		return false;
	}
	
	public int hashCode(){
		return ("" + id + application + deploymentModel).hashCode() + slos.hashCode();
	}
	
	public String toString(){
		return "SuccessfulDeployment(id=" + id + ", application=" + application + ", deploymentModel=" + deploymentModel + ", itSlos=" + slos + ", executionContexts=" + executionContexts; 
	}
	
}
