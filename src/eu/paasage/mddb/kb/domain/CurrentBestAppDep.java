/* Copyright (C) 2015 KYRIAKOS KRITIKOS <kritikos@ics.forth.gr> */

/* This Source Code Form is subject to the terms of the Mozilla Public 
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/ 
 */

package eu.paasage.mddb.kb.domain;

import java.util.Set;

import javax.xml.bind.annotation.XmlAnyElement;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import eu.paasage.camel.Application;
import eu.paasage.camel.impl.ApplicationImpl;
import eu.paasage.camel.metric.Metric;

@XmlRootElement
public class CurrentBestAppDep implements java.io.Serializable{

	private static final long serialVersionUID = 3905168554525941766L;
	//@XmlTransient
	private static int idGen = 1;
	private int id;
	private Application application;
	private Set<Metric> metrics;
	private double value;
	private SuccAppDep deployment;
	
	public CurrentBestAppDep(){
		id = idGen++;
	}
	
	public CurrentBestAppDep(int id){
		this.id = id;
	}
	
	public CurrentBestAppDep(Application application, SuccAppDep deployment, Set<Metric> metrics, Double value){
		this.id = idGen++;
		this.application = application;
		this.metrics = metrics;
		this.deployment = deployment;
		this.value = value;
	}
	
	@XmlAttribute
	public int getId(){
		return id;
	}
	
	public void setId(int id){
		this.id = id;
	}
	
	@XmlElement(type=ApplicationImpl.class)
	public Application getApplication(){
		return application;
	}
	
	public void setApplication(Application application){
		this.application = application;
	}
	
	@XmlAnyElement
	public Set<Metric> getMetrics(){
		return metrics;
	}
	
	public void setMetric(Set<Metric> metrics){
		this.metrics = metrics;
	}
	
	@XmlAnyElement
	public SuccAppDep getDeployment(){
		return deployment;
	}
	
	public void setDeployment(SuccAppDep deployment){
		this.deployment = deployment;
	}
	
	@XmlAnyElement
	public double getValue(){
		return value;
	}
	
	public void setValue(Double value){
		this.value = value;
	}
	
	
	public boolean equals(Object o){
		if (o instanceof CurrentBestAppDep){
			CurrentBestAppDep bda = (CurrentBestAppDep)o;
			if (bda.getApplication().equals(application) && bda.getMetrics().equals(metrics) && bda.getValue() == value) return true;
		}
		return false;
	}
	
	public int hashCode(){
		return ("" + id + application.getName()).hashCode() + metrics.hashCode() + (value+"").hashCode();
	}
	
}
