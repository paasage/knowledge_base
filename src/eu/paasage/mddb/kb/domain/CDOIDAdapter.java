/* Copyright (C) 2015 KYRIAKOS KRITIKOS <kritikos@ics.forth.gr> */

/* This Source Code Form is subject to the terms of the Mozilla Public 
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/ 
 */

package eu.paasage.mddb.kb.domain;

import javax.xml.bind.annotation.adapters.XmlAdapter;

import org.eclipse.emf.cdo.common.id.CDOID;
import org.eclipse.emf.cdo.internal.common.id.CDOIDObjectLongWithClassifierImpl;

public class CDOIDAdapter
    extends XmlAdapter<String, CDOID>
{

	@Override
    public CDOID unmarshal(String string) throws Exception {
        return (CDOID)(CDOIDObjectLongWithClassifierImpl.create(string));
    }

    @Override
    public String marshal(CDOID id) throws Exception {
        if (id == null) return "";
        else return id.toURIFragment();
    }

}
