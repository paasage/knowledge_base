/* Copyright (C) 2015 KYRIAKOS KRITIKOS <kritikos@ics.forth.gr> */

/* This Source Code Form is subject to the terms of the Mozilla Public 
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/ 
 */

package eu.paasage.mddb.kb.domain;

import java.util.Set;

import javax.xml.bind.annotation.XmlAnyElement;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;

import org.eclipse.emf.cdo.common.id.CDOID;

@XmlRootElement
public class ApplicationMatch implements java.io.Serializable{
	private static final long serialVersionUID = 8394482408256109607L;
	private CDOID firstApplicationID;
	private CDOID secondApplicationID;
	private ApplicationMatching applicationMatching;
	private Set<ComponentMatch> componentMatches;
	
	public enum ApplicationMatching{
		EQUIVALENT,
		SIMILAR
	}
	
	public ApplicationMatch(){
		
	}
	
	public ApplicationMatch(CDOID firstApplicationID, CDOID secondApplicationID, ApplicationMatching applicationMatching){
		this.firstApplicationID = firstApplicationID;
		this.secondApplicationID = secondApplicationID;
		this.applicationMatching = applicationMatching;
	}
	
	public void setFirstApplicationID(CDOID firstApplicationID){
		this.firstApplicationID = firstApplicationID;
	}
	
	@XmlAttribute
	public CDOID getFirstApplicationID(){
		return firstApplicationID;
	}
	
	public void setSecondApplicationID(CDOID secondApplicationID){
		this.secondApplicationID = secondApplicationID;
	}
	
	@XmlAttribute
	public CDOID getSecondApplicationID(){
		return secondApplicationID;
	}
	
	public void setApplicationMatching(ApplicationMatching applicationMatching){
		this.applicationMatching = applicationMatching;
	}
	
	@XmlAttribute
	public ApplicationMatching getApplicationMatching(){
		return applicationMatching;
	}
	
	public void setArtifactMatches(Set<ComponentMatch> componentMatches){
		this.componentMatches = componentMatches;
	}
	
	@XmlAnyElement
	public Set<ComponentMatch> getComponentMatches(){
		return componentMatches;
	}
	
	public String toString(){
		return "Application Match: ( " + firstApplicationID + ", " + secondApplicationID + ") with type:" + applicationMatching;
	}
	
	public boolean equals(Object o){
		if (o instanceof ApplicationMatch){
			ApplicationMatch am = (ApplicationMatch)o;
			if (am.firstApplicationID == firstApplicationID && am.secondApplicationID == secondApplicationID && am.applicationMatching == applicationMatching) return true;
		}
		return false;
	}
	
	public int hashCode(){
		return 10 * firstApplicationID.hashCode() + 1000 * secondApplicationID.hashCode() + 100 * ("" + applicationMatching).hashCode(); 
	}
}
