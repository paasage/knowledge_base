/* Copyright (C) 2015 KYRIAKOS KRITIKOS <kritikos@ics.forth.gr> */

/* This Source Code Form is subject to the terms of the Mozilla Public 
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/ 
 */

package eu.paasage.mddb.kb.domain;

import java.util.Set;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;

import org.eclipse.emf.cdo.common.id.CDOID;

@XmlRootElement
public class BestComponentDeployment implements java.io.Serializable{
	private static final long serialVersionUID = 4559968178891710364L;
	//@XmlTransient
	private static int idGen = 1;
	private int id;
	private CDOID component;
	private Set<CDOID> metrics;
	private CDOID hostingComponent;
	
	public BestComponentDeployment(){
		id = idGen++;
	}
	
	public BestComponentDeployment(int id){
		this.id = id;
	}
	
	public BestComponentDeployment(int id, CDOID component, Set<CDOID> metrics, CDOID hostingComponent){
		this.id = id;
		this.component = component;
		this.metrics = metrics;
		this.hostingComponent = hostingComponent;
	}
	
	@XmlAttribute
	public int getId(){
		return id;
	}
	
	public void setId(int id){
		this.id = id;
	}
	
	@XmlAttribute
	public CDOID getComponent(){
		return component;
	}
	
	public void setComponent(CDOID component){
		this.component = component;
	}
	
	//@XmlAnyAttribute
	public Set<CDOID> getMetrics(){
		return metrics;
	}
	
	public void setMetrics(Set<CDOID> metrics){
		this.metrics = metrics;
	}
	
	@XmlAttribute
	public CDOID getHostingComponent(){
		return hostingComponent;
	}
	
	public void setHostingComponent(CDOID hostingComponent){
		this.hostingComponent = hostingComponent;
	}
	
	
	public boolean equals(Object o){
		if (o instanceof BestComponentDeployment){
			BestComponentDeployment bda = (BestComponentDeployment)o;
			if (bda.getComponent().equals(component) && bda.getMetrics().equals(metrics) && bda.getHostingComponent().equals(hostingComponent)) return true;
		}
		return false;
	}
	
	public int hashCode(){
		return ("" + id + component).hashCode() + metrics.hashCode() + hostingComponent.hashCode();
	}
	
}
