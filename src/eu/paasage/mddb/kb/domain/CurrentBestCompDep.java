/* Copyright (C) 2015 KYRIAKOS KRITIKOS <kritikos@ics.forth.gr> */

/* This Source Code Form is subject to the terms of the Mozilla Public 
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/ 
 */

package eu.paasage.mddb.kb.domain;

import javax.xml.bind.annotation.XmlAnyElement;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import eu.paasage.camel.deployment.Component;
import eu.paasage.camel.deployment.InternalComponent;
import eu.paasage.camel.deployment.impl.InternalComponentImpl;
import eu.paasage.camel.metric.Metric;

@XmlRootElement
public class CurrentBestCompDep implements java.io.Serializable{
	
	private static final long serialVersionUID = -3764012901866621506L;
	//@XmlTransient
	private static int idGen = 1;
	private int id;
	private InternalComponent component;
	private Component hostingComponent;
	private Metric metric;
	private double value;
	private SuccCompDep deployment;
	
	public CurrentBestCompDep(){
		id = idGen++;
	}
	
	public CurrentBestCompDep(int id){
		this.id = id;
	}
	
	public CurrentBestCompDep(InternalComponent component, Component hostingComponent, SuccCompDep deployment, Metric metric, Double value){
		this.id = idGen++;
		this.component = component;
		this.hostingComponent = hostingComponent;
		this.metric = metric;
		this.deployment = deployment;
		this.value = value;
	}
	
	@XmlAttribute
	public int getId(){
		return id;
	}
	
	public void setId(int id){
		this.id = id;
	}
	
	@XmlElement(type=InternalComponentImpl.class)
	public InternalComponent getComponent(){
		return component;
	}
	
	public void setComponent(InternalComponent component){
		this.component = component;
	}

	@XmlElement(type=InternalComponentImpl.class)
	public Component getHostingComponent(){
		return hostingComponent;
	}
	
	public void setHostingComponent(Component hostingComponent){
		this.hostingComponent = hostingComponent;
	}
	
	@XmlAnyElement
	public Metric getMetric(){
		return metric;
	}
	
	public void setMetric(Metric metric){
		this.metric = metric;
	}
	
	@XmlAnyElement
	public SuccCompDep getDeployment(){
		return deployment;
	}
	
	public void setDeployment(SuccCompDep deployment){
		this.deployment = deployment;
	}
	
	@XmlAnyElement
	public double getValue(){
		return value;
	}
	
	public void setValue(Double value){
		this.value = value;
	}
	
	
	public boolean equals(Object o){
		if (o instanceof CurrentBestCompDep){
			CurrentBestCompDep bda = (CurrentBestCompDep)o;
			if (bda.getComponent().getName().equals(component.getName()) && bda.getHostingComponent().getName().equals(hostingComponent.getName())&& bda.getMetric().equals(metric) && bda.getValue() == value) return true;
		}
		return false;
	}
	
	public int hashCode(){
		if (hostingComponent == null)
			return ("" + id + component.getName()).hashCode() + metric.hashCode() + (value+"").hashCode();
		else
			return ("" + id + component.getName() + hostingComponent.getName()).hashCode() + metric.hashCode() + (value+"").hashCode();
	}
	
}
