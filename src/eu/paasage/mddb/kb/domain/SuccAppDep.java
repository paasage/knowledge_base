/* Copyright (C) 2015 KYRIAKOS KRITIKOS <kritikos@ics.forth.gr> */

/* This Source Code Form is subject to the terms of the Mozilla Public 
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/ 
 */

package eu.paasage.mddb.kb.domain;

import java.util.Set;

import javax.xml.bind.annotation.XmlAnyElement;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import eu.paasage.camel.Application;
import eu.paasage.camel.deployment.DeploymentModel;
import eu.paasage.camel.execution.ExecutionContext;
import eu.paasage.camel.impl.ApplicationImpl;
import eu.paasage.camel.requirement.ServiceLevelObjective;

@XmlRootElement
public class SuccAppDep implements java.io.Serializable{
	private static final long serialVersionUID = 7773720276292951572L;
	private static int idGen = 1;
	private int id;
	private Application application;
	private Set<ServiceLevelObjective> slos;
	private DeploymentModel deploymentModel;
	private Set<ExecutionContext> executionContexts;
	//private Set<ApplMonitor> applMonitors;
	
	public SuccAppDep(){
		id = idGen++;
	}
	
	public SuccAppDep(int id){
		this.id = id;
	}
	
	public SuccAppDep(int id, Application application, Set<ServiceLevelObjective> slos, DeploymentModel dm){
		this.id = id;
		this.application = application;
		this.slos = slos;
		this.deploymentModel = dm;
	}
	
	@XmlAttribute
	public int getId(){
		return id;
	}
	
	public void setId(int id){
		this.id = id;
	}
	
	@XmlElement(type=ApplicationImpl.class)
	public Application getApplication(){
		return application;
	}
	
	public void setApplication(Application application){
		this.application = application;
	}
	
	@XmlAnyElement
	public Set<ServiceLevelObjective> getSlos(){
		return slos;
	}
	
	public void setSlos(Set<ServiceLevelObjective> slos){
		this.slos = slos;
	}
	
	@XmlAnyElement
	public Set<ExecutionContext> getExecutionContexts(){
		return executionContexts;
	}
	
	public void setExecutionContexts(Set<ExecutionContext> executionContexts){
		this.executionContexts = executionContexts;
	}
	
	@XmlAnyElement
	public DeploymentModel getDeploymentModel(){
		return deploymentModel;
	}
	
	public void setDeploymentModel(DeploymentModel deploymentModel){
		this.deploymentModel = deploymentModel;
	}
	
	public boolean equals(Object o){
		if (o instanceof SuccAppDep){
			SuccAppDep bda = (SuccAppDep)o;
			if (bda.getApplication().equals(application) && bda.getSlos().equals(slos) && bda.getDeploymentModel().equals(deploymentModel)) return true;
		}
		return false;
	}
	
	public int hashCode(){
		return ("" + id + application.getName() + deploymentModel.getName()).hashCode() + slos.hashCode();
	}
	
	public String toString(){
		return "SuccessfulDeployment(id=" + id + ", application=" + application + ", deploymentModel=" + deploymentModel + ", itSlos=" + slos + ", executionContexts=" + executionContexts; 
	}
	
}
