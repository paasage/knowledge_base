/* Copyright (C) 2015 KYRIAKOS KRITIKOS <kritikos@ics.forth.gr> */

/* This Source Code Form is subject to the terms of the Mozilla Public 
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/ 
 */

package eu.paasage.mddb.kb.domain;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class MyQueryResults implements java.io.Serializable{
	private static final long serialVersionUID = 2574838798727860642L;
	private List<MyResultRow> results = null;
	private List<String> identifiers = null;
	
	public MyQueryResults(){
		results = new ArrayList<MyResultRow>();
		identifiers = new ArrayList<String>();
	}
	
	@XmlElement
	public List<MyResultRow> getResults(){
		return results;
	}
	
	public void setResults(List<MyResultRow> results){
		this.results = results;
	}
	
	@XmlElement
	public List<String> getIdentifiers(){
		return identifiers;
	}
	
	public void setIdentifiers(List<String> identifiers){
		this.identifiers = identifiers;
	}

}