/* Copyright (C) 2015 KYRIAKOS KRITIKOS <kritikos@ics.forth.gr> */

/* This Source Code Form is subject to the terms of the Mozilla Public 
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/ 
 */

package eu.paasage.mddb.kb.domain;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;

import org.eclipse.emf.cdo.common.id.CDOID;

@XmlRootElement
public class ComponentMatch implements java.io.Serializable{

	private static final long serialVersionUID = 3828557746660285259L;
	private CDOID firstInternalComponentID;
	private CDOID secondInternalComponentID;
	private long id;
	
	public ComponentMatch(){
		
	}
	
	public ComponentMatch(long id){
		this.id = id;
	}
	
	public ComponentMatch(CDOID firstInternalComponentID, CDOID secondInternalComponentID){
		this.firstInternalComponentID = firstInternalComponentID;
		this.secondInternalComponentID = secondInternalComponentID;
	}
	
	public void setFirstInternalComponentID(CDOID firstInternalComponentID){
		this.firstInternalComponentID = firstInternalComponentID;
	}
	
	@XmlAttribute
	public CDOID getFirstInternalComponentID(){
		return firstInternalComponentID;
	}
	
	public void setSecondInternalComponentID(CDOID secondInternalComponentID){
		this.secondInternalComponentID = secondInternalComponentID;
	}
	
	@XmlAttribute
	public CDOID getSecondInternalComponentID(){
		return secondInternalComponentID;
	}
	
	public void setId(long id){
		this.id = id;
	}
	
	@XmlAttribute
	public long getId(){
		return id;
	}
	
	public String toString(){
		return "InternalComponentMatch: (" + firstInternalComponentID + ", " + secondInternalComponentID + ")";
	}
	
	public boolean equals(Object o){
		if (o instanceof ComponentMatch){
			ComponentMatch am = (ComponentMatch)o;
			if (am.firstInternalComponentID.equals(firstInternalComponentID) && am.secondInternalComponentID.equals(secondInternalComponentID)) return true;
		}
		return false;
	}
	
	public int hashCode(){
		return 10 * firstInternalComponentID.hashCode() + 1000 * secondInternalComponentID.hashCode(); 
	}
}
