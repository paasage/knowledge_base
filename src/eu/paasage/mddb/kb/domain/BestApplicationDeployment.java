/* Copyright (C) 2015 KYRIAKOS KRITIKOS <kritikos@ics.forth.gr> */

/* This Source Code Form is subject to the terms of the Mozilla Public 
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/ 
 */

package eu.paasage.mddb.kb.domain;

import java.util.Set;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;

import org.eclipse.emf.cdo.common.id.CDOID;

@XmlRootElement
public class BestApplicationDeployment implements java.io.Serializable{
	private static final long serialVersionUID = 158832831419983013L;
	//@XmlTransient
	private static int idGen = 1;
	private int id;
	private CDOID application;
	private Set<CDOID> metrics;
	private CDOID deploymentModel;
	
	public BestApplicationDeployment(){
		id = idGen++;
	}
	
	public BestApplicationDeployment(int id){
		this.id = id;
	}
	
	public BestApplicationDeployment(int id, CDOID application, Set<CDOID> metrics, CDOID deploymentModel){
		this.id = id;
		this.application = application;
		this.metrics = metrics;
		this.deploymentModel = deploymentModel;
	}
	
	@XmlAttribute
	public int getId(){
		return id;
	}
	
	public void setId(int id){
		this.id = id;
	}
	
	@XmlAttribute
	public CDOID getApplication(){
		return application;
	}
	
	public void setApplication(CDOID application){
		this.application = application;
	}
	
	//@XmlAnyAttribute
	public Set<CDOID> getMetrics(){
		return metrics;
	}
	
	public void setMetrics(Set<CDOID> metrics){
		this.metrics = metrics;
	}
	
	@XmlAttribute
	public CDOID getDeploymentModel(){
		return deploymentModel;
	}
	
	public void setDeploymentModel(CDOID deploymentModel){
		this.deploymentModel = deploymentModel;
	}
	
	
	public boolean equals(Object o){
		if (o instanceof BestApplicationDeployment){
			BestApplicationDeployment bda = (BestApplicationDeployment)o;
			if (bda.getApplication().equals(application) && bda.getMetrics().equals(metrics) && bda.getDeploymentModel().equals(deploymentModel)) return true;
		}
		return false;
	}
	
	public int hashCode(){
		return ("" + id + application).hashCode() /*+ itSlos.hashCode()*/ + deploymentModel.hashCode();
	}
	
}
