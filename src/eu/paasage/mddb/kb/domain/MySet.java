/* Copyright (C) 2015 KYRIAKOS KRITIKOS <kritikos@ics.forth.gr> */

/* This Source Code Form is subject to the terms of the Mozilla Public 
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/ 
 */

package eu.paasage.mddb.kb.domain;

import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

import javax.xml.bind.annotation.XmlAnyElement;
import javax.xml.bind.annotation.XmlType;

//@XmlRootElement
@XmlType(propOrder = { "set" })
public class MySet<E> implements Set<E>, java.io.Serializable{
	
	private static final long serialVersionUID = -6064484142213590928L;
	private Set<E> set = new HashSet<E>();
	
	public MySet(){
		
	}
	
	public MySet(MySet<E> set){
		this.set = set;
	}
	
	public boolean add(E e){
		return set.add(e);
	}
	
	public void clear(){
		set.clear();
	}
	
	public boolean isEmpty(){
		return set.isEmpty();
	}
	
	public boolean contains(Object o){
		return set.contains(o);
	}
	
	public boolean containsAll(Collection<?> c){
		return set.containsAll(c);
	}
	
	public boolean equals(Object o){
		return set.equals(o);
	}
	
	public int hashCode(){
		return set.hashCode();
	}
	
	public Iterator<E> iterator(){
		return set.iterator();
	}
	
	public boolean remove(Object o){
		return set.remove(o);
	}
	
	public boolean removeAll(Collection<?> c){
		return set.removeAll(c);
	}
	
	public boolean retainAll(Collection<?> c){
		return set.retainAll(c);
	}
	
	public int size(){
		return set.size();
	}
	
	
	public Object[] toArray(){
		return set.toArray();
	}

	public <T> T[] toArray(T[] a) {
		return set.toArray(a);
	}

	public boolean addAll(Collection<? extends E> c) {
		return set.addAll(c);
	}
	
	@XmlAnyElement(lax=true)
	public Set<E> getSet(){
		return set;
	}
	
	public void setSet(Set<E> set){
		this.set = set;
	}
	
}