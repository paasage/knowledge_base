/* Copyright (C) 2015 KYRIAKOS KRITIKOS <kritikos@ics.forth.gr> */

/* This Source Code Form is subject to the terms of the Mozilla Public 
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/ 
 */

package eu.paasage.mddb.kb.domain;

import java.util.List;

import org.eclipse.emf.cdo.CDOObject;
import org.eclipse.emf.cdo.common.id.CDOID;
import org.eclipse.emf.cdo.view.CDOView;

import eu.paasage.camel.metric.Metric;
import eu.paasage.camel.metric.MetricCondition;
import eu.paasage.camel.requirement.ServiceLevelObjective;
import eu.paasage.mddb.cdo.client.CDOClient;

public final class CDOUtils {
	public static boolean sameMetric(CDOID id1, CDOID id2, CDOView view){
		ServiceLevelObjective slo1 = (ServiceLevelObjective)view.getObject(id1);
		ServiceLevelObjective slo2 = (ServiceLevelObjective)view.getObject(id2);
		Metric m1 = ((MetricCondition)slo1.getCustomServiceLevel()).getMetricContext().getMetric();
		Metric m2 = ((MetricCondition)slo2.getCustomServiceLevel()).getMetricContext().getMetric();
		return m1.equals(m2);
	}
	
	public static boolean subsumes(CDOID id1, CDOID id2, CDOView view){
		ServiceLevelObjective slo1 = (ServiceLevelObjective)view.getObject(id1);
		ServiceLevelObjective slo2 = (ServiceLevelObjective)view.getObject(id2);
		MetricCondition mc1 = ((MetricCondition)slo1.getCustomServiceLevel());
		MetricCondition mc2 = ((MetricCondition)slo2.getCustomServiceLevel());
		Metric m = mc1.getMetricContext().getMetric();
		short direction1 = m.getValueDirection();
		double value1 = mc1.getThreshold();
		double value2 = mc2.getThreshold();
		if (direction1 == (short)1) return (value1 >= value2);
		else return (value1 <= value2);
	}
	
	public static CDOID getObject(String name, String className){
		CDOClient cl = new CDOClient();
		CDOView view = cl.openView();
		CDOID id = null;
		List<CDOObject> result = view.createQuery("hql", "select o from " + className + " o where o.name='" + name + "'").getResult(CDOObject.class);
		if (result != null && !result.isEmpty()) id = result.get(0).cdoID();
		view.close();
		cl.closeSession();
		return id;
	}
}
