/* Copyright (C) 2015 KYRIAKOS KRITIKOS <kritikos@ics.forth.gr> */

/* This Source Code Form is subject to the terms of the Mozilla Public 
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/ 
 */

package eu.paasage.mddb.kb.domain;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class ErrorItem {

    private Integer errorCode = 400;
    private String name = "bad_parameter";
    private String description = "A wrong parameter value was provided";
    
    ErrorItem() {}

    public ErrorItem(Integer errorCode, String name, String description) {
        this.errorCode = errorCode;
        this.name = name;
        this.description = description;
    }
    
    public Integer getErrorCode(){
    	return errorCode;
    }
    
    public void setErrorCode(Integer errorCode){
    	this.errorCode = errorCode;
    }
    
    public String getName(){
    	return name;
    }
    
    public void setName(String name){
    	this.name = name;
    }
    
    public String getDescription(){
    	return description;
    }
    
    public void setDescription(String description){
    	this.description = description;
    }
    
    public String toString(){
    	return name + " " + description;
    }
}