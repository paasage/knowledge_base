/* Copyright (C) 2015 KYRIAKOS KRITIKOS <kritikos@ics.forth.gr> */

/* This Source Code Form is subject to the terms of the Mozilla Public 
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/ 
 */

package eu.paasage.mddb.kb.domain;

import java.util.Set;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import org.eclipse.emf.cdo.common.id.CDOID;

@XmlRootElement
public class SuccessfulComponentDeployment implements java.io.Serializable{
	private static final long serialVersionUID = 2608166055139844771L;
	private static int idGen = 1;
	private int id;
	private CDOID component;
	private CDOID hostingComponent;
	private Set<CDOID> slos;
	private CDOID executionContextID;
	
	public SuccessfulComponentDeployment(){
		id = idGen++;
	}
	
	public SuccessfulComponentDeployment(int id){
		this.id = id;
	}
	
	public SuccessfulComponentDeployment(int id, CDOID component, CDOID hostingComponent, Set<CDOID> slos, CDOID executionContextID){
		this.id = id;
		this.component = component;
		this.hostingComponent = hostingComponent;
		this.slos = slos;
		this.executionContextID = executionContextID;
	}
	
	@XmlAttribute
	public int getId(){
		return id;
	}
	
	public void setId(int id){
		this.id = id;
	}
	
	@XmlAttribute
	public CDOID getComponent(){
		return component;
	}
	
	public void setComponent(CDOID component){
		this.component = component;
	}
	
	@XmlAttribute
	public CDOID getHostingComponent(){
		return hostingComponent;
	}
	
	public void setHostingComponent(CDOID hostingComponent){
		this.hostingComponent = hostingComponent;
	}
	
	public Set<CDOID> getSlos(){
		return slos;
	}
	
	public void setSlos(Set<CDOID> slos){
		this.slos = slos;
	}
	
	@XmlElement
	public CDOID getExecutionContextID(){
		return executionContextID;
	}
	
	public void setExecutionContextID(CDOID executionContextID){
		this.executionContextID = executionContextID;
	}
	
	public boolean equals(Object o){
		if (o instanceof SuccessfulComponentDeployment){
			SuccessfulComponentDeployment bda = (SuccessfulComponentDeployment)o;
			if (bda.getComponent().equals(component) && bda.getHostingComponent().equals(hostingComponent) && bda.getSlos().equals(slos)) return true;
		}
		return false;
	}
	
	public int hashCode(){
		if (slos != null)
			return ("" + id + component + hostingComponent).hashCode() + slos.hashCode();
		else
			return ("" + id + component + hostingComponent).hashCode();
	}
	
	public String toString(){
		return "SuccessfulDeployment(id=" + id + ", component=" + component + ", hostingComponent=" + hostingComponent + ", executionContext=" + executionContextID + ", slos=" + slos; 
	}
	
}
