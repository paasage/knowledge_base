/* Copyright (C) 2015 KYRIAKOS KRITIKOS <kritikos@ics.forth.gr> */

/* This Source Code Form is subject to the terms of the Mozilla Public 
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/ 
 */

package eu.paasage.mddb.kb.domain;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSeeAlso;

import org.eclipse.emf.cdo.common.id.CDOID;

@XmlSeeAlso({Wildcard.class,MySet.class,CDOIDSet.class})
@XmlRootElement
public class MyQueryParameter {
	private String basicObject;
	private Object derivedObject;
	private CDOID cdoID;
	
	public MyQueryParameter(){
		
	}
	
	public void setBasicObject(String basicObject){
		this.basicObject = basicObject;
	}
	
	public void setDerivedObject(Object derivedObject){
		this.derivedObject = derivedObject;
	}
	
	public void setCdoID(CDOID cdoID){
		this.cdoID = cdoID;
	}
	
	@XmlAttribute
	public String getBasicObject(){
		return basicObject;
	}
	
	@XmlAttribute
	public CDOID getCdoID(){
		return cdoID;
	}
	
	@XmlElement
	public Object getDerivedObject(){
		return derivedObject;
	}
	
}