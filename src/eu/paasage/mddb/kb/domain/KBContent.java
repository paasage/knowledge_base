/* Copyright (C) 2015 KYRIAKOS KRITIKOS <kritikos@ics.forth.gr> */

/* This Source Code Form is subject to the terms of the Mozilla Public 
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/ 
 */

package eu.paasage.mddb.kb.domain;

import java.util.List;
import java.util.Map;

import javax.xml.bind.annotation.XmlAnyAttribute;
import javax.xml.bind.annotation.XmlAnyElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.namespace.QName;

@XmlSeeAlso({ComponentMatch.class,ApplicationMatch.class,BestComponentDeployment.class,BestApplicationDeployment.class,SuccessfulApplicationDeployment.class,SuccessfulComponentDeployment.class,MySet.class})
@XmlRootElement
public class KBContent implements java.io.Serializable{
	private static final long serialVersionUID = 8052618392095145784L;
		List elements = null;
		Map<QName, Object> attributes = null;
		
		public KBContent(){
			
		}
		
		public KBContent(List elements, Map<QName, Object> attributes){
			this.elements = elements;
			this.attributes = attributes;
		}
		
		public void setElements(List elements){
			this.elements = elements;
		}
		
		@XmlAnyElement
		public List getElements(){
			return elements;
		}
		

		public void setAttributes(Map<QName, Object> attributes){
			this.attributes = attributes;
		}
		
		@XmlAnyAttribute
		public Map<QName, Object> getAttributes(){
	        return attributes;
		}
}