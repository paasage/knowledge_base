/* Copyright (C) 2015 KYRIAKOS KRITIKOS <kritikos@ics.forth.gr> */

/* This Source Code Form is subject to the terms of the Mozilla Public 
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/ 
 */

package eu.paasage.mddb.kb.domain;

import java.util.Hashtable;

import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSeeAlso;

@XmlSeeAlso({ComponentMatch.class,ApplicationMatch.class,BestComponentDeployment.class,BestApplicationDeployment.class,MySet.class})
@XmlRootElement
public class MyResultRow implements java.io.Serializable{
	private static final long serialVersionUID = 1812674935297127176L;
	private Hashtable<String,MyResultColumn> row;
	
	public MyResultRow(){
		row = new Hashtable<String,MyResultColumn>();
	}
	
	//@XmlJavaTypeAdapter(HashMapAdapter.class)
	//@XmlAnyAttribute
	public Hashtable<String,MyResultColumn> getRow(){
		return row;
	}
	
	public void setRow(Hashtable<String,MyResultColumn> row){
		this.row = row;
	}
		
	public MyResultColumn getResult(String identifier){
		if (row.isEmpty()) return null;
		return row.get(identifier);
	}

}