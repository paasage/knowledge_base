/* Copyright (C) 2015 KYRIAKOS KRITIKOS <kritikos@ics.forth.gr> */

/* This Source Code Form is subject to the terms of the Mozilla Public 
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/ 
 */

@XmlJavaTypeAdapters({
@XmlJavaTypeAdapter(value=IntegerAdapter.class, type=Integer.class),
@XmlJavaTypeAdapter(value=CDOIDAdapter.class, type=CDOID.class)
})
package eu.paasage.mddb.kb.domain;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapters;

import org.eclipse.emf.cdo.common.id.CDOID;

