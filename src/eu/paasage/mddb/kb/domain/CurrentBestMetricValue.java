/* Copyright (C) 2015 KYRIAKOS KRITIKOS <kritikos@ics.forth.gr> */

/* This Source Code Form is subject to the terms of the Mozilla Public 
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/ 
 */

package eu.paasage.mddb.kb.domain;

import javax.xml.bind.annotation.XmlAnyElement;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import eu.paasage.camel.Application;
import eu.paasage.camel.impl.ApplicationImpl;
import eu.paasage.camel.metric.Metric;

@XmlRootElement
public class CurrentBestMetricValue implements java.io.Serializable{

	private static final long serialVersionUID = -6040848806094556866L;
	//@XmlTransient
	private static int idGen = 1;
	private int id;
	private Application application;
	private Metric metric;
	private double value;
	
	public CurrentBestMetricValue(){
		id = idGen++;
	}
	
	public CurrentBestMetricValue(int id){
		this.id = id;
	}
	
	public CurrentBestMetricValue(Application application, Metric metric, Double value){
		this.id = idGen++;
		this.application = application;
		this.metric = metric;
		this.value = value;
	}
	
	@XmlAttribute
	public int getId(){
		return id;
	}
	
	public void setId(int id){
		this.id = id;
	}
	
	@XmlElement(type=ApplicationImpl.class)
	public Application getApplication(){
		return application;
	}
	
	public void setApplication(Application application){
		this.application = application;
	}
	
	@XmlAnyElement
	public Metric getMetric(){
		return metric;
	}
	
	public void setMetric(Metric metric){
		this.metric = metric;
	}
		
	@XmlAnyElement
	public double getValue(){
		return value;
	}
	
	public void setValue(Double value){
		this.value = value;
	}
	
	
	public boolean equals(Object o){
		if (o instanceof CurrentBestMetricValue){
			CurrentBestMetricValue bda = (CurrentBestMetricValue)o;
			if (bda.getApplication().equals(application) && bda.getMetric().equals(metric) && bda.getValue() == value) return true;
		}
		return false;
	}
	
	public int hashCode(){
		return ("" + id + application.getName()).hashCode() + metric.hashCode() + (value+"").hashCode();
	}
	
}
