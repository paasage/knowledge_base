/* Copyright (C) 2015 KYRIAKOS KRITIKOS <kritikos@ics.forth.gr> */

/* This Source Code Form is subject to the terms of the Mozilla Public 
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/ 
 */

package eu.paasage.mddb.kb.domain;

import java.util.Set;

import javax.xml.bind.annotation.XmlAnyElement;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;

import org.eclipse.emf.cdo.common.id.CDOID;
import org.eclipse.emf.cdo.spi.common.id.AbstractCDOID;

import eu.paasage.camel.deployment.Component;
import eu.paasage.camel.deployment.InternalComponent;
import eu.paasage.camel.deployment.impl.InternalComponentImpl;
import eu.paasage.camel.requirement.ServiceLevelObjective;

public class SuccCompDep implements java.io.Serializable{
	private static final long serialVersionUID = -5422793494698151486L;
	private static int idGen = 1;
	private int id;
	private InternalComponent component;
	private Component hostingComponent;
	private Set<ServiceLevelObjective> slos;
	private CDOID executionContextID;
	
	public SuccCompDep(){
		id = idGen++;
	}
	
	public SuccCompDep(int id){
		this.id = id;
	}
	
	public SuccCompDep(int id, InternalComponent component, Component hostingComponent, Set<ServiceLevelObjective> slos, CDOID executionContextID){
		this.id = id;
		this.component = component;
		this.hostingComponent = hostingComponent;
		this.slos = slos;
		this.executionContextID = executionContextID;
	}
	
	@XmlAttribute
	public int getId(){
		return id;
	}
	
	public void setId(int id){
		this.id = id;
	}
	
	@XmlElement(type=InternalComponentImpl.class)
	public InternalComponent getComponent(){
		return component;
	}
	
	public void setComponent(InternalComponent component){
		this.component = component;
	}
	
	@XmlElement(type=InternalComponentImpl.class)
	public Component getHostingComponent(){
		return hostingComponent;
	}
	
	public void setHostingComponent(Component hostingComponent){
		this.hostingComponent = hostingComponent;
	}
	
	@XmlAnyElement
	public Set<ServiceLevelObjective> getSlos(){
		return slos;
	}
	
	public void setSlos(Set<ServiceLevelObjective> slos){
		this.slos = slos;
	}
	
	@XmlElement(type=AbstractCDOID.class)
	public CDOID getExecutionContextID(){
		return executionContextID;
	}
	
	public void setExecutionContextID(CDOID executionContextID){
		this.executionContextID = executionContextID;
	}
	
	public boolean equals(Object o){
		if (o instanceof SuccCompDep){
			SuccCompDep bda = (SuccCompDep)o;
			if (bda.getComponent().getName().equals(component.getName()) && bda.getHostingComponent().getName().equals(hostingComponent.getName()) && bda.getSlos().equals(slos)) return true;
		}
		return false;
	}
	
	public int hashCode(){
		return ("" + id + component.getName() + hostingComponent.getName()).hashCode() + slos.hashCode();
	}
	
	public String toString(){
		return "SuccessfulDeployment(id=" + id + ", component=" + component + ", hostingComponent=" + hostingComponent + ", executionContext=" + executionContextID + ", slos=" + slos; 
	}
	
}
